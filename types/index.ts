export type inProduct = {
  id?: number
  reference: string
  prix: number
  nom?: string
  description?: string
  images?: string
}

export type Product = {
  nom: string | undefined;
  categorie?: string | undefined;
  images: string | string[];
  taille: string;
  couleur: string;
  description: string | undefined;
  id?: number | undefined;
  reference: string;
  prix: number;
}

export type FormScrape = {
  service: string
  inFile: string
  outFile: string
  delay: number
  limitTo: number
  limitFrom: number
  headless: boolean
  cancel: boolean
}
