import { asideAtom } from "@/atoms";
import { useAtom } from "jotai";
import Weather from "./Weather";

export default function MainContent({ children }: any) {
  const [isClosed, setIsClosed] = useAtom(asideAtom);

  return <main style={{ width: isClosed ? '100%' : 'calc(100% - 250px)', marginLeft: isClosed ? '0' : '250px' }}>
    <header className="d-flex justify-between align-center p-3">
      <div className="d-flex align-center">
        <button className="bg-inherit white p-0 border-0 cp" onClick={() => { setIsClosed(!isClosed) }}>
          <h1 className="m-0"><i className="fa fa-stream"></i></h1>
        </button>
      </div>

      <div className="d-flex align-center">
        <Weather />
        <h1 className="m-0 ml-2"><i className="fa fa-search"></i></h1>
        <h1 className="m-0 ml-2"><i className="fa fa-bell"></i></h1>
        <h1 className="m-0 ml-2"><img className="object-cover rounded" src="avatar.jpeg" alt="" width={30} height={30} /></h1>
      </div>
    </header>
    <section className="p-3 pt-0">{children}</section>
  </main>
}