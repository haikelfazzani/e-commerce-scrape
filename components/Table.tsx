import { showTableSelectedItemImageAtom, tableSelectedItemAtom } from "@/atoms";
import { useAtom } from "jotai";
import styles from "@/styles/Table.module.css";

export default function Table({
  productsSliced,
  onProduct,
  onInput,
  contentEditable,
  onDeleteItem,
}: {
  productsSliced: any[],
  onProduct: any,
  onInput: any,
  contentEditable: boolean,
  onDeleteItem: Function
}) {

  if (!productsSliced || productsSliced.length < 1) return <></>;

  const [showImg, setShowImg] = useAtom(showTableSelectedItemImageAtom);
  const [tableSlectedItem] = useAtom(tableSelectedItemAtom);
  const founded = productsSliced.find((p: any) => p.images || p.description) || productsSliced[0];

  return <div className={'w-100 p-2 ' + styles.table} style={{ maxHeight: '500px' }}>
    <table className='w-100 overflow br7'>
      <thead>
        <tr>
          <th>#</th>
          {Object.keys(founded).map((p: any, i) => <th
            key={i} style={{ width: p === 'reference' || p === 'variant' ? '150px' : '50px' }}>{p}</th>)}
          <th>Action</th>
        </tr>
      </thead>
      <tbody>
        {productsSliced.filter(v => v).map((p: any, i: number) => <tr
          key={i + 'id'}
          onClick={() => { onProduct(p) }}
          className={(tableSlectedItem && tableSlectedItem.reference) && tableSlectedItem.reference === p.reference ? "bg-metal cp" : "cp"}
        >
          <td>{i + 1}</td>
          {Object.keys(founded).map((k, indx) => <td
            onInput={(e) => { onInput(e, p, i, k) }}
            title={p ? ''+p[k] : ''}
            key={indx}
            contentEditable={contentEditable}
            suppressContentEditableWarning
            tabIndex={-1}>
            {p ? p[k] : ''}
          </td>)}

          <td>
            {p.images && <button className="mr-1" onClick={() => { setShowImg(!showImg) }}><i className="fa fa-image"></i></button>}
            <button className="red" onClick={() => { onDeleteItem(p, i) }}><i className="fa fa-trash"></i></button>
          </td>

        </tr>)}
      </tbody>
    </table>

  </div>
}