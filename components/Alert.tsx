import { useEffect, useState } from "react"

export default function Alert({ children, clx="", isOpen = false, color = "ochre" }: any) {
  const [show, setShow] = useState(isOpen);

  useEffect(() => {
    setShow(isOpen);
  }, [isOpen]);

  const onToggle = () => {
    setShow(!show)
  }

  if (!show) return <></>

  return (
    <div className={`w-100 shadow alert d-flex justify-between br7 mb-2 bordered bg-${color} ${clx}`}>
      <div><i className="fa fa-info-circle mr-1"></i>{children}</div>
      <span onClick={onToggle} style={{fontSize:'22px', padding:0,backgroundColor:'transparent',cursor:'pointer'}}>&times;</span>
    </div>
  )
}
