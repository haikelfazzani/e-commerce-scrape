import React from 'react'
import Sidebar from './Sidebar'
import MainContent from './MainContent'

export default function Wrapper({ children }: any) {
  return (
    <>
      <Sidebar />
      <MainContent>{children}</MainContent>
    </>
  )
}
