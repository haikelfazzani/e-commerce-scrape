import { tableSelectedItemAtom } from "@/atoms";
import jsonToCSVorXLSX from "@/utils/jsonToCSVorXLSX";
import { useAtom } from "jotai";
import dynamic from "next/dynamic";
import { useEffect, useState } from "react";
import Table from "./Table";
import fetchJson from "@/utils/fetchJson";
import { toast, Toaster } from "sonner";

const ProductCard = dynamic(() => import("./ProductCard"), { ssr: false });

let pslice = 50;
let psliceMarge = 50;

type Props = {
  products: [],
  file: string,
  contentEditable?: boolean,
  inDir?: string,
  fileExt?: string,
  options?: { contentEditable?: boolean, reset?: boolean, setVariant?: boolean }
}

export default function TableView({
  products,
  file,
  contentEditable = false,
  inDir = 'OUT',
  fileExt = 'xlsx',
  options = { contentEditable: false, reset: true, setVariant: true }
}: Props) {
  const [productsTemp, setProductsTemp] = useState([]);
  const [productsSliced, setProductsSliced] = useState([]);
  const [tableSlectedItem, setTableSlectedItem] = useAtom(tableSelectedItemAtom)
  const [reference, setReference] = useState('');
  const [isContentEditable, setIsContentEditable] = useState(contentEditable);

  useEffect(() => {
    if (products && products.length > 0) {
      setProductsSliced(products.slice(0, pslice))
      setProductsTemp(products);
      setTableSlectedItem(products[0]);
    }
  }, [products]);

  if (productsTemp && productsTemp.length > 0) {

    const onSearch = (e: any) => {
      e.preventDefault();
      const founded = productsTemp.filter((p: any) => p.reference.includes(reference) || p.reference.startsWith(reference));
      setProductsSliced(founded.length < 1 ? productsTemp : founded);
      if (founded.length < 1) setProductsSliced(productsTemp.slice(0, psliceMarge));
    }

    const onInput = (e: any, p: any, index: number, key: string) => {
      const temp: any = productsTemp.slice(0);
      temp[index][key] = e.target.textContent;
      setProductsTemp(temp);
      setProductsSliced(temp.slice(0, psliceMarge));
    }

    const onDownload = async (e: any) => {
      e.preventDefault();
      const filename = e.target.elements[0].value;
      const fileType = e.target.elements[1].value;

      const pds = productsTemp.map((v: any) => {
        if (v.images && Array.isArray(v.images)) {
          v.images = v.images.join('///');
          return v;
        }
        return v;
      });

      await jsonToCSVorXLSX(pds, filename, fileType);
    }

    const onCopy = async () => {
      const pds = productsTemp.map((v: any) => {
        if (v.images && Array.isArray(v.images)) {
          v.images = v.images.join('///');
          return v;
        }
        return v;
      });

      toast.promise(navigator.clipboard.writeText(JSON.stringify(pds)), {
        loading: 'Please Patient, copied to clipboard...',
        success: () => {          
          return `Products are copied to clipboard`;
        },
        error: 'Error to copy products to clipboard',
      });
    }

    const onDeleteImg = (index: number) => {
      if (window.confirm('Do you really want to delete this image?') && tableSlectedItem.images) {
        const product = { ...tableSlectedItem };

        product.images = (Array.isArray(tableSlectedItem.images) ? tableSlectedItem.images : tableSlectedItem.images.split('///'))
          .filter((v: any, i: number) => i !== index);

        const pds: any = productsTemp.map((p: any) => {
          if (p.reference === product.reference) {
            p.images = product.images;
            return p;
          }
          else return p
        });

        setProductsTemp(pds);
        setProductsSliced(pds.slice(0, psliceMarge));
        setTableSlectedItem(product);
      }
    }

    const onLoadProducts = (status: string) => {
      if (status && status === 'more') { pslice = pslice + psliceMarge; setProductsSliced(productsTemp.slice(0, pslice)) }
      else { pslice = pslice - psliceMarge; setProductsSliced(productsTemp.slice(0, pslice)) }
    }

    const onDeleteItem = (product: any, index: number) => {
      if (window.confirm(`Do you really want to delete? [${index + 1}] ${product.reference}`)) {
        const filtred = productsTemp.filter((p: any) => p.reference && p.reference !== product.reference);
        setProductsSliced(filtred.slice(0, 50));
        setProductsTemp(filtred);
      }
    }

    const onSetVariant = async () => {
      if (window.confirm('Do you really want to set variant for? ' + file)) {
        toast.promise(fetchJson(`/api/variant?file=${file}&fileExt=${fileExt}&inDir=${inDir}`), {
          loading: 'Please Patient, Set Variant To Products...',
          success: (products) => {
            setProductsTemp(products);
            setProductsSliced(products.slice(0, psliceMarge));
            return `Set Variant Done For ${products.length} Products`;
          },
          error: 'Error',
        });
      }
    }

    const onResetChanges = async () => {
      if (window.confirm('Do you really want to reset changes for? ' + file)) {
        try {
          const products = await fetchJson(`/api/files?file=${file}&type=file&job=fetch&fileExt=${fileExt}&inDir=${inDir}`);
          setProductsTemp(products);
          setProductsSliced(products.slice(0, psliceMarge));
        } catch (error) {

        }
      }
    }

    return <div className="br7">
      <Toaster position="bottom-right" richColors theme="dark" visibleToasts={7} />

      <div className="bg-dark shadow mt-3">
        <p className="mt-0 p-1 pr-2 pl-2 border-bottom"><i className="fa fa-table mr-1"></i>"{file}" file contains {products.length} product(s)</p>

        <div className="d-flex p-2 pb-0">
          <form className="w-100 d-flex" onSubmit={onSearch}>
            <input className="w-100 mr-1" type="search" name="reference" id="reference"
              onChange={(e) => { setReference(e.target.value.trim()); }} value={reference} placeholder="reference" />
            <button className="btn"><i className="fa fa-search"></i></button>
          </form>

          <button className="btn bg-yellow ml-1" onClick={() => { setIsContentEditable(!isContentEditable) }}
            title="Edit Table Columns">
            <i className={isContentEditable ? "fa fa-exclamation-triangle" : "fa fa-edit"}></i>
          </button>

          {options.reset && <button type="button" className="btn bg-red ml-1" title="reset changes" onClick={onResetChanges}>
            <i className="fa fa-recycle"></i>
          </button>}

          {options.setVariant && <button className='btn bg-green ml-1' type='button' title="Set Variant" onClick={onSetVariant}>
            <i className="fas fa-sort-numeric-up"></i>
          </button>}
        </div>

        <ProductCard onDelete={onDeleteImg} />

        <Table
          productsSliced={productsSliced}
          onDeleteItem={onDeleteItem}
          onInput={onInput}
          onProduct={setTableSlectedItem}
          contentEditable={isContentEditable}
        />

        <div className="d-flex mt-1 p-2">
          {products.length > 49 && <button className="w-100 btn bg-gray shadow mr-1" onClick={() => { onLoadProducts('more') }}><i className="fa fa-chevron-circle-down mr-1"></i>Load More Products ({pslice})</button>}
          {productsSliced.length > 98 && <button className="w-100 btn bg-gray shadow ml-1" onClick={() => { onLoadProducts('less') }}><i className="fa fa-chevron-circle-up mr-1"></i>Load Less Products ({pslice})</button>}
        </div>
      </div>

      <form className="d-flex shadow p-2 border-top" onSubmit={onDownload}>
        <input className="w-100 mr-1" type="text" name="filename" id="filename" placeholder="petit-bateau" required />
        <select className="mr-1" name="fileType" id="fileType" required><option value="csv">CSV</option><option value="xlsx">Excel</option></select>
        <button type="submit" className="btn mr-1"><i className="fa fa-download mr-1"></i>download</button>
        <button type="button" className="btn" onClick={onCopy}><i className="fa fa-copy mr-1"></i>copy as json</button>
      </form>
    </div>
  }
  else return <></>
}
