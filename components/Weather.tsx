import WeatherService from '@/services/WeatherService';
import React, { useEffect, useState } from 'react'

export default function Weather() {
  const isIpad = typeof window !== 'undefined' ? window.innerWidth < 1025 : false;
  const [state, setState]: any = useState(null)

  useEffect(() => {
    if(!isIpad) WeatherService.getData().then(data => { setState(data.current); });
  }, []);

  if (state && state.condition && !isIpad) {
    return (
      <div className='bg-dark d-flex align-center br7'>
        <p className='m-0 ml-1'>{state.temp_c}°C / {state.wind_kph}kph</p>
        <img src={state.condition.icon} alt={state.condition.text} width="40" />
      </div>
    )
  }
  else return <></>
}