import styles from "@/styles/Select.module.css";
import { memo, useState } from "react";

type Select = {
  data: string[]
  onChange: any
  value: string
  clx?: string
  name?: string
  label?: React.ReactNode
  cleanName?: boolean
}

function Select({
  data,
  onChange,
  value = '',
  clx = '',
  name = 'inFile',
  label = 'In File',
  cleanName = false
}: Select) {

  const [state, setState] = useState(false);

  const onItem = (value: string) => {
    onChange({ target: { value, name } });
    setState(!state);
  }

  return <div className={clx + ' w-100 ' + styles.drop_list}>
    <label htmlFor={name}>{label}</label>
    <button type="button" className='w-100 cp' onClick={() => { setState(!state) }}>
      {cleanName ? value.replaceAll('-', ' ').toUpperCase() : value}
    </button>

    <ul className={'w-100 bg-white shadow br7 ' + (state ? 'translateX' : 'd-none')}>
      {data && data.map((f: string, i: number) => <li key={i}
        className={'w-100 d-flex justify-between align-center p-05 cp border-bottom ' + (value === f ? 'bg-dark' : '')}
        onClick={() => { onItem(f) }}>
        <span><i className="fas fa-chevron-right mr-1"></i>{f}</span>
        <i className={value === f ? 'fa fa-check green ml-1' : 'ml-1'}></i>
      </li>)}
    </ul>
  </div>
}

export default memo(Select)
