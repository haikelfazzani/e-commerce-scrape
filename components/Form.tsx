export default function Form({ children, onSubmit, className = "shadow p-2 br7 mb-3" }: any) {
  return (
    <form className={className} onSubmit={onSubmit}>{children}</form>
  )
}
