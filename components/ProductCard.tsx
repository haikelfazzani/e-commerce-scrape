import { useMemo, useState } from 'react'
import { showTableSelectedItemImageAtom, tableSelectedItemAtom } from '@/atoms';
import { useAtom } from 'jotai';
import styles from "@/styles/ProductCard.module.css";

export default function ProductCard({ onDelete }: any) {
  const [index, setIndex] = useState(0);
  const [tableSlectedItem] = useAtom(tableSelectedItemAtom)
  const [showImg, setShowImg] = useAtom(showTableSelectedItemImageAtom);

  const images = useMemo(() => {
    if (tableSlectedItem && tableSlectedItem.images) {
      return Array.isArray(tableSlectedItem.images)
        ? tableSlectedItem.images
        : (tableSlectedItem.images as string).split('///')
    }
    return null;
  }, [tableSlectedItem]);

  
  if (!tableSlectedItem || !images || images.length < 1 || !showImg) return <></>
  
  const onPrev = () => {
    setIndex(prev => prev <= 0 ? images!.length - 1 : prev - 1)
  }

  const onNext = () => {
    setIndex(prev => prev >= images!.length - 1 ? 0 : prev + 1)
  }

  return <div className={styles.card_product + ' ' + styles.box + " shadow br7 scale"}>
    <div className="w-100 p-1 left" style={{ height: '145px' }}>
      <h4 className='m-0 lines' title={tableSlectedItem.nom}>{tableSlectedItem.nom}</h4>
      <p className='blue mt-1' title={tableSlectedItem.reference}>{tableSlectedItem.reference}</p>
      <p className='metal lines m-0 mt-1' title={tableSlectedItem.description}>{tableSlectedItem.description}</p>
    </div>

    <div className='d-flex flex-column justify-between' style={{ height: 'calc(100% - 145px)' }}>
      <div className="p-1 d-flex flex-column">
        <a title='Click on image to open in new tab' href={images[index]} target="_blank">
          <img className="w-100 shadow br7" src={images[index]} alt={tableSlectedItem.nom} />
        </a>
      </div>

      <div className={styles.card_product_footer + ' w-100 d-flex justify-between'}>
        <button title='Previous Image' onClick={onPrev}><i className='fa fa-arrow-left'></i></button>
        <button>{index + 1}/{images.length}</button>
        <button title='Next Image' onClick={onNext}><i className='fa fa-arrow-right'></i></button>
        <button title='Delete' className='red' onClick={() => { onDelete(index) }}><i className='fa fa-trash'></i></button>
        <button title='Close' className="red border-right-0" onClick={() => { setShowImg(!showImg) }}><i className="fa fa-times"></i></button>
      </div>
    </div>
  </div>
}
