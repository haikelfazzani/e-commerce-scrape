import { asideAtom } from "@/atoms";
import { useAtom } from "jotai";
import Link from "next/link";
import styles from "../styles/Sidebar.module.css";
import Image from 'next/image'

export default function Sidebar() {
  const [isClosed] = useAtom(asideAtom);

  return <aside className={styles.sidebar} style={{ transform: `translateX(${isClosed ? '-100%' : '0'})` }}>
    <div className={styles.sidebar_content}>
      <header className="d-flex align-center p-3 pl-3 mb-3">
        <Image
          className='object-cover mr-1 br7'
          src="/image.png"
          alt="Lebsa Kids"
          width={35}
          height={35}
        />

        <div className="d-flex flex-column m-0 uppercase">
          <h2 className="m-0">Almeria</h2>
          <small>Scrape</small>
        </div>
      </header>

      <nav className='w-100 pl-3 pr-3'>
        <ul className='w-100'>
          <li className='mb-3'><i className="fa fa-home mr-1"></i><Link href="/">dashboard</Link></li>
          <li className='mb-3'><i className="fa fa-user mr-1"></i><Link href="/account">account</Link></li>
          <li className='mb-3'><i className="fa fa-play mr-1"></i><Link href="/runs">Runs</Link></li>
          <li className='mb-3'><i className="fa fa-database mr-1"></i><Link href="/storage">storage</Link></li>
          <li className='mb-3'><i className="fa fa-upload mr-1"></i><Link href="/upload-file">Upload</Link></li>
          <li className='mb-3'><i className="fa fa-clock mr-1"></i><Link href="/history">History</Link></li>
          <li className='mb-3'><i className="fa fa-cog mr-1"></i><Link href="/settings">Settings</Link></li>
        </ul>
      </nav>

      <div className="d-flex flex-column pr-3 pl-3 mt-3 pb-2 uppercase">
        <h3 className="m-0 mb-1">Almeria ©</h3>
        <small>All Rights Reserved 2023</small>
      </div>
    </div>
  </aside>
}
