import fetchJson from "@/utils/fetchJson";
import { useEffect, useState } from "react";

export default function useFetch<T>(defaultValue: any, url: string, options?: object) {
  const [data, setData] = useState<T>(defaultValue);

  useEffect(() => {
    let abortController = new AbortController();
    fetchJson(url, { ...options, signal: abortController.signal }).then(setData);

    return () => {
      abortController.abort();
    }
  }, [url, options])

  return [data, setData]
}