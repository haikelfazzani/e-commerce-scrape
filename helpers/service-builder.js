const { workerData } = require('worker_threads');
const BirkenstockService = require('./services/BirkenstockService');
const PetitBateauFranceService = require('./services/PetitBateauFranceService');
const GimelTunisieService = require('./services/GimelTunisieService');
const PetitBateauTunisieService = require('./services/PetitBateauTunisieService');
const GioseppotunisieService = require('./services/GioseppotunisieService');
const DPMService = require('./services/DPMService');
const TooptyService = require('./services/TooptyService');
const SergentmajorService = require('./services/SergentmajorService');
const MayoralService = require('./services/MayoralService');
const KiabiService = require('./services/KiabiService');
const OkaidiService = require('./services/OkaidiService');
const CitymodeService = require('./services/CitymodeService');

const getService = (service) => {
  switch (service) {
    case 'petit-bateau-france': return PetitBateauFranceService;
    case 'petit-bateau-tunisie': return PetitBateauTunisieService;
    case 'gimel-tunisie': return GimelTunisieService;
    case 'toopty': return TooptyService;
    case 'sergent-major': return SergentmajorService;
    case 'birkenstock': return BirkenstockService;
    case 'mayoral': return MayoralService;
    case 'gioseppotunisie': return GioseppotunisieService;
    case 'dpam': return DPMService;
    case 'kiabi': return KiabiService;
    case 'okaidi': return OkaidiService;
    case 'citymode': return CitymodeService;

    default:
      break;
  }
}

(async () => {
  const ServiceBuilder = getService(workerData.service);
  const builder = new ServiceBuilder(workerData.puppeteerConfig, workerData.config, workerData.products);
  await builder.init();
  await builder.run();
})();

