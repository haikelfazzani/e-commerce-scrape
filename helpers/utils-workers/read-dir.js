const { workerData, parentPort } = require("worker_threads");
const readDirectory = require("../utils/readDirectory");

(async () => {
  const data = await readDirectory(workerData.inDir);
  parentPort.postMessage(data);
})();