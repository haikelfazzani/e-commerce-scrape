const { workerData, parentPort } = require("worker_threads");
const setVariant = require("../utils/setVariant");

(async () => {
  const data = await setVariant(workerData.filePath);
  parentPort.postMessage(data);
})();