const { workerData, parentPort } = require("worker_threads");
const writeToProgressFile = require("../utils/updateProgressFile");

(async () => {
  const data = await writeToProgressFile(workerData);
  parentPort.postMessage(data);
})();