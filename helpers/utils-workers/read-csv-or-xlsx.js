const { workerData, parentPort } = require("worker_threads");
const readCSVorXLSX = require("../utils/readCSVorXLSX");

(async () => {
  const data = await readCSVorXLSX(workerData);
  parentPort.postMessage(data);
})();