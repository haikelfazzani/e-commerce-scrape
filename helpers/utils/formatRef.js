module.exports = function formatRef(ref) { // 1366-55 => 01366-055
  let [code, num] = ref.split(/\/|-/g);
  code = code.length === 3 ? '00' + code : code.length === 4 ? '0' + code : code;
  num = num.length === 2 ? '0' + num : num.length === 1 ? '00' + num : num;
  return code + '-' + num
}