const { join } = require("path");
const readXlsxFile = require("read-excel-file/node");
const { BASE_IN_CSV_FILES_PATH, BASE_OUT_CSV_FILES_PATH } = require("./constants");
const csvToJsonConvert = require("./csvToJsonConvert");
const matriceToObject = require("./matriceToObject");

module.exports = async function readCSVorXLSX({ file, service, isInFile }) {
  let products;
  let columns = [];
  const filePath = isInFile ? join(BASE_IN_CSV_FILES_PATH, file) : join(BASE_OUT_CSV_FILES_PATH, file);

  if (file.endsWith('csv')) {
    const result = await csvToJsonConvert(filePath);
    products = result[0];
    columns = result[1].map((v) => v.replace(/\s+/g, '_').toLowerCase());
  }
  else {
    const data = await readXlsxFile(filePath);
    columns = data[0].map((v) => {
      if (v) {
        if (/NOM_PRODUIT/gi.test(v)) v = 'nom';
        return v.replace(/\s+/g, '_').toLowerCase();
      }
      return v
    });
    products = matriceToObject(columns, data.filter((v) => v).slice(1));
  }

  // if (service && service === 'mayoral') {
  //   const result = [];
  //   products.forEach(p => {
  //     if (!result.some((r) => p.reference.includes(r.reference) || p.reference === r.reference)) result.push(p);
  //   });
  //   products = result.slice(0);
  // }

  return products;
}
