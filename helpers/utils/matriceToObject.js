/**
const columns = ['id', 'reference'];
const data = [[1, '54687'], [2, '687'], [3, 546]];


[
  { id: 1, reference: '54687' },
  { id: 2, reference: '687' },
  { id: 3, reference: '546' }
]
 */

module.exports = function matriceToObject(columns, data) {
  return data.map((row) => {
    const mergedObject = {};
    columns.forEach((column, index) => {
      mergedObject[column] = /reference/gi.test(column) ? '' + row[index] : row[index];
    });
    return mergedObject;
  });
}
