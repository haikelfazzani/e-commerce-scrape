const { writeFile } = require("fs/promises");
const colors = require("./colors");

module.exports = async function updateProgressFile({ inProduct = null, done = false, outFile = '', message }) {
  const content = require(process.cwd() + '/_private/_scrape_logs.json');
  if (inProduct) content.products.push(inProduct);

  if (message) {
    content.message = message;
    console.log(colors.FgYellow, message, colors.Reset);
  }

  content.done = done;
  content.outFile = outFile;
  content.columns = [];
  writeFile(process.cwd() + '/_private/_scrape_logs.json', JSON.stringify(content));
}