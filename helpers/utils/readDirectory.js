const { readdir } = require("fs/promises");
const { BASE_IN_CSV_FILES_PATH, BASE_OUT_CSV_FILES_PATH } = require("./constants");

module.exports = async function readDirectory(inDir = 'IN') {
  console.log(inDir);
  const allFiles = await readdir(inDir === 'IN' ? BASE_IN_CSV_FILES_PATH : BASE_OUT_CSV_FILES_PATH);
  return allFiles.filter(f => f.endsWith('.csv') || f.endsWith('.xlsx'));
}