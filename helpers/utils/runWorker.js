const { Worker } = require("worker_threads");

module.exports = function runWorkerPromise(filePath, workerData) {
  return new Promise((resolve, reject) => {
    const worker = new Worker(filePath, workerData);
    worker.on('message', resolve);
    worker.on('error', reject);
    worker.on('exit', reject);
  });
}