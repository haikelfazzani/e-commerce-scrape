const { csvToJson } = require("convert-csv-to-json/src/csvToJson");
const { readFile } = require("fs/promises");

function csvToJSON(csvString) {
  if (!csvString) return null;

  const rows = csvString.split('\n');
  const headers = rows[0].split(',');
  const jsonData = [];

  for (let i = 1; i < rows.length; i++) {
    const values = rows[i].split(',');
    const rowObject = {};

    for (let j = 0; j < headers.length; j++) {
      rowObject[headers[j]] = values[j];
    }

    jsonData.push(rowObject);
  }

  return [jsonData, headers];
}

module.exports = async function csvToJsonConvert(filePath) {
  try {
    const products = csvToJson.fieldDelimiter(',').formatValueByType().parseSubArray("*", ',').supportQuotedField(true).getJsonFromCsv(filePath);
    const columns = Object.keys(products[0]);
    return [products, columns];
  } catch (error) {
    const data = await readFile(filePath, { encoding: 'utf8' });
    return csvToJSON(data);
  }
}