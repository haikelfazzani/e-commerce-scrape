const readXlsxFile = require("read-excel-file/node");
const { readFile, writeFile } = require('fs/promises');
const short = require('short-uuid');
const { json2csv } = require("json-2-csv");

async function setVariantCSV(filePath) {
  const data = await readFile(filePath.replace('.csv', '.json'), { encoding: 'utf8' });
  const products = JSON.parse(data);
  const result = [];

  products.forEach((d, i) => {
    const reference = /-/gi.test(d.reference) ? d.reference.split('-')[0] : d.reference;
    const variant = short.generate().slice(0, 6);
    const regex = new RegExp(reference, 'gi');
    const founded = result.find((k) => regex.test(k.reference) || reference === k.reference);
    result.push({ ...d, variant: result.length > 0 && founded ? founded.variant : variant });
  });

  return result
}

async function setVariantXLSX(filePath) {
  const mergedArray = (columns, res) => {
    return res.map((item) => {
      const obj = {};
      for (let i = 0; i < columns.length; i++) {
        // @ts-ignore: Unreachable code error
        obj[columns[i]] = item[i];
      }
      return obj;
    });
  }

  const fileContent = await readXlsxFile(filePath);
  const colunms = fileContent[0];
  const refIndex = colunms.indexOf('reference');
  const data = fileContent.slice(1);
  let result = [];

  data.forEach((d, i) => {
    const reference = /-/gi.test(d[refIndex]) ? d[refIndex].split('-')[0] : d[refIndex];
    const variant = short.generate().slice(0, 6);
    const regex = new RegExp(reference, 'gi');

    if (!result.find((k) => regex.test(k))) {
      d[0] = variant;
      result = [...result, d];
    }
    else {
      const founded = result.find((v) => regex.test(v));
      d[0] = founded[0];
      result = [...result, d];
    }
  });

  return mergedArray(colunms, result)
}

module.exports = async function setVariant(filePath) {
  try {
    const result = filePath.endsWith('.csv') ? await setVariantCSV(filePath) : await setVariantXLSX(filePath);

    // @ts-ignore: Unreachable code error
    const csv = await json2csv(result);
    if (csv) {
      writeFile(filePath, csv);
      writeFile(filePath.replace(/.csv|.xlsx/g, '.json'), JSON.stringify(result));
    }
    return result;
  } catch (error) {
    console.log(error.message);
    return `Error to set variant for "${filePath}" file\n (${error.message})`;
  }
}