const { writeFile } = require("fs/promises");
const { json2csv } = require("json-2-csv");
const { BASE_OUT_CSV_FILES_PATH, BASE_IN_CSV_FILES_PATH } = require("./constants");

module.exports = async function createFile({
  data,
  filename,
  type = 'json',
  inOutPAth = true
}) {
  let csv;
  if (type === 'csv') csv = await json2csv(data);
  const pth = inOutPAth ? BASE_OUT_CSV_FILES_PATH : BASE_IN_CSV_FILES_PATH;
  await writeFile(`${pth}/${filename}.${type}`, type === 'csv' ? csv : JSON.stringify(data));
}