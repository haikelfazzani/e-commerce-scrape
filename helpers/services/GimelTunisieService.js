const colors = require('../utils/colors');
const { BASE_CANCEL_SCRAPE_FILE_PATH, BASE_OUT_CSV_FILES_PATH } = require('../utils/constants');
const { readFile } = require('fs/promises');
const puppeteer = require('puppeteer');
const updateProgressFile = require('../utils/updateProgressFile');
const createFile = require('../utils/createFile');

const url = 'https://www.gimel-tunisie.com';
const temp_url = 'https://www.gimel-tunisie.com/accueil/3735-23150-robe-pour-bebe-en-organza-brode.html#/26-taille-6_mois/248-couleur-rosepale';

module.exports = class GimelTunisieService {
  puppeteerConfig;
  config;
  timeoutId;
  browser;
  page;
  outFileCSVPath;
  products;
  counter = 1;
  outFileJsonPath;
  outProducts = [];

  constructor(puppeteerConfig, config, products) {
    this.puppeteerConfig = puppeteerConfig;
    this.config = config;
    this.timeoutId = null;
    this.outFileCSVPath = `${BASE_OUT_CSV_FILES_PATH}/${config.outFile}.csv`;
    this.outFileJsonPath = `${BASE_OUT_CSV_FILES_PATH}/${config.outFile}.json`;
    this.products = products;
  }

  async init() {
    this.browser = await puppeteer.launch(this.puppeteerConfig);
    await this.browser.newPage();
    const pages = await this.browser.pages()
    this.page = pages[0];
    await this.page.setViewport({ width: 576, height: 631 });
    // await this.page.setRequestInterception(true);

    // this.page.on('request', (request) => {
    //   const url = request.url();
    //   const isInBlackList = blocked_domains.some(domain => url.includes(domain));
    //   const isBlockedResource = ['media', 'texttrack', 'object', 'beacon', 'csp_report',].includes(request.resourceType());
    //   if (isBlockedResource || isInBlackList || url.endsWith('.gif') || url.endsWith('.svg') || url.endsWith('.ico')) request.abort();
    //   else request.continue();
    // });

    await this.page.goto(temp_url);
  }

  async scrape(inProduct) {
    try {
      if (!inProduct || !inProduct.reference) { return null; }
      await new Promise((resolve) => setTimeout(resolve, 200));
      await this.page.click('#show_search');
      await this.page.waitForSelector('.block_content.clearfix');
      await this.page.$eval('input.search_query', el => el.value = '');
      await this.page.type('input.search_query', inProduct.reference);
      await this.page.waitForSelector('.ac_results.lps_results', { visible: true });

      const listItems = await this.page.evaluate(() => {
        const listItems = document.querySelectorAll('.ac_results.lps_results ul li');
        if (listItems.length > 0) { listItems[0].click(); return 1; }
        else return 0;
      });

      if (listItems.length < 1) return inProduct;

      await this.page.waitForNavigation();
      await this.page.waitForSelector('#thumb-gallery');

      const product = await this.page.evaluate((inProduct) => {
        const ref = document.querySelector('.product-reference span[itemprop="sku"]').textContent;
        if (!ref.includes(inProduct.reference)) return inProduct;

        return {
          ...inProduct,
          nom: document.querySelector('h1.product-detail-name').textContent.trim(),
          prix: document.querySelector('.current-price span[itemprop="price"]').textContent.replace(/\s+dt/gi, ''),
          description: document.querySelector('.product-description').textContent.trim(),
          images: [...document.querySelectorAll('#thumb-gallery img')].map(v => v.dataset.imageLargeSrc).filter(v => v).join('///')
        }
      }, inProduct);

      return product;
    } catch (error) {
      console.log(colors.FgMagenta, '[Error] Scrape method ', error.message, colors.Reset);
      return inProduct;
    }
  }

  async run() {
    const { outFile, productsLen } = this.config;
    let counter = 0;
    console.log('> [Start setInterval]', counter);

    const handler = async () => {
      try {
        const isCanceled = await readFile(BASE_CANCEL_SCRAPE_FILE_PATH, { encoding: 'utf8' });
        if (counter >= productsLen || isCanceled === 'true') {
          this.browser.close();
          clearInterval(this.timeoutId);
          await createFile({ filename: outFile, data: this.outProducts.slice(0, -1), type: 'csv' });
          console.log('> [End setInterval]', counter);
          return;
        }
        else {
          counter++;
          const inProduct = this.products[counter];
          const response = await this.scrape(inProduct);
          this.outProducts = [...this.outProducts, response];
          response.done = response && (response.images || response.description) ? true : false;
          updateProgressFile({ outFile: this.config.outFile, inProduct: response, message: `[${response.done ? 'Done' : 'Not Found'}] Product: ${response.reference} (${counter}/${productsLen})` });
          await createFile({ filename: outFile, data: this.outProducts });
        }
      } catch (error) {
        this.browser.close();
        await updateProgressFile({ error: error.message, config: this.config, done: true });
        clearInterval(this.timeoutId);
        console.log('> (SetInterval Exit Error)', error);
      }
    }

    if (this.browser.isConnected()) this.timeoutId = setInterval(handler, this.config.delay * 1000);
  }
}