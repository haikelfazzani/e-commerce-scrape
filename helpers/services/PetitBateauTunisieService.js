const colors = require('../utils/colors');
const { BASE_CANCEL_SCRAPE_FILE_PATH, BASE_OUT_CSV_FILES_PATH, blocked_domains } = require('../utils/constants');
const { readFile } = require('fs/promises');
const puppeteer = require('puppeteer');
const updateProgressFile = require('../utils/updateProgressFile');
const createFile = require('../utils/createFile');

const url = 'https://www.petit-bateau.tn';
const url_search_params = '/instantsearch/result/?q=';

module.exports = class PetitBateauTunisieService {
  puppeteerConfig;
  config;
  timeoutId;
  browser;
  page;
  outFileCSVPath;
  products;
  counter = 1;
  outFileJsonPath;
  outProducts = [];

  constructor(puppeteerConfig, config, products) {
    this.puppeteerConfig = puppeteerConfig;
    this.config = config;
    this.timeoutId = null;
    this.outFileCSVPath = `${BASE_OUT_CSV_FILES_PATH}/${config.outFile}.csv`;
    this.outFileJsonPath = `${BASE_OUT_CSV_FILES_PATH}/${config.outFile}.json`;
    this.products = products;
  }

  async init() {
    this.browser = await puppeteer.launch(this.puppeteerConfig);
    await this.browser.newPage();
    const pages = await this.browser.pages()
    this.page = pages[0];
    await this.page.setRequestInterception(true);
    // this.page.setJavaScriptEnabled(false);

    this.page.on('request', (request) => {
      const url = request.url();
      const isInBlackList = blocked_domains.some(domain => url.includes(domain));
      const isBlockedResource = ['stylesheet', 'media', 'font', 'texttrack', 'object', 'beacon', 'csp_report',].includes(request.resourceType());
      if (isBlockedResource || isInBlackList || url.endsWith('.gif') || url.endsWith('.svg') || url.endsWith('.ico')) request.abort();
      else request.continue();
    });

    await this.page.goto(url + url_search_params + '4619701');
  }

  async scrape(inProduct) {
    try {
      if (!inProduct || !inProduct.reference) { return null; }

      await this.page.goto(url + url_search_params + inProduct.reference, { waitUntil: 'domcontentloaded', });
      await this.page.waitForSelector('#maincontent');

      const listItems = await this.page.$$eval('ol.products.list.items.product-items li', li => li.length);
      if (listItems < 1) return inProduct;

      const isProductFound = await this.page.evaluate((reference) => {
        const isFound = [...document.querySelectorAll('ol.products.list.items.product-items li a')].find(link => {
          // https://www.petit-bateau.tn/bandeau-4955301.html
          const arr = link.href.replace('.html', '').split('/');
          const nameArr = arr[arr.length - 1].split('-');
          const ref = nameArr[nameArr.length - 1];
          return reference.toLowerCase().trim() === ref.toLowerCase().trim()
        });
        if (!isFound) return 0;
        isFound.click();
        return 1;
      }, inProduct.reference);

      if (isProductFound < 1) return inProduct;

      const [response] = await Promise.all([
        await this.page.waitForNavigation(),
        await this.page.waitForSelector('.item.gallery-item'),
        await this.page.click('.product.item-image.imgzoom img'),
      ]);

      if (!response) {
        this.page.$eval(`.product.item-image.imgzoom img`, element => element.click());
      }

      const product = await this.page.evaluate((inProduct) => new Promise((resolve) => {
        let nb = document.querySelector('.mfp-counter'),
          cnt = nb ? +(nb.textContent || '1 of 6').match(/(\d+)(?!.*\d)/g) : 6,
          images = [];

        const id = setInterval(() => {
          images.push(document.querySelector('img.mfp-img').src);
          document.querySelector('button.mfp-arrow.mfp-arrow-right').click();
          if (cnt < 2) {
            clearInterval(id);
            resolve({
              ...inProduct,
              images: images.join('///'),
              nom: document.querySelector('.product-name').textContent.trim(),
              description: document.getElementById('description').textContent.trim(),
              prix_scrape: document.querySelector('[data-price-amount]').dataset.priceAmount
            });
          }
          cnt--;
        }, 500 / cnt);
      }), inProduct);

      return { ...inProduct, ...product };
    } catch (error) {
      console.log(colors.FgMagenta, '[Error] Scrape method ', error.message, colors.Reset);
      return inProduct;
    }
  }

  async run() {
    const { outFile, productsLen } = this.config;
    let counter = 0;
    console.log('> [Start setInterval]', counter);

    const handler = async () => {
      try {
        const isCanceled = await readFile(BASE_CANCEL_SCRAPE_FILE_PATH, { encoding: 'utf8' });
        if (counter >= productsLen || isCanceled === 'true') {
          this.browser.close();
          clearInterval(this.timeoutId);
          await createFile({ filename: outFile, data: this.outProducts.slice(0, -1), type: 'csv' });
          console.log('> [End setInterval]', counter);
          return;
        }
        else {
          counter++;
          const inProduct = this.products[counter];
          const response = await this.scrape(inProduct);
          this.outProducts = [...this.outProducts, response];
          response.done = response && (response.images || response.description) ? true : false;
          updateProgressFile({ outFile: this.config.outFile, inProduct: response, message: `[${response.done ? 'Done' : 'Not Found'}] Product: ${response.reference} (${counter}/${productsLen})` });
          await createFile({ filename: outFile, data: this.outProducts });
        }
      } catch (error) {
        this.browser.close();
        await updateProgressFile({ error: error.message, config: this.config, done: true });
        clearInterval(this.timeoutId);
        console.log('> (SetInterval Exit Error)', error);
      }
    }

    if (this.browser.isConnected()) this.timeoutId = setInterval(handler, this.config.delay * 1000);
  }
}