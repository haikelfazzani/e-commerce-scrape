const colors = require('../utils/colors');
const { BASE_CANCEL_SCRAPE_FILE_PATH, BASE_OUT_CSV_FILES_PATH } = require('../utils/constants');
const { readFile } = require('fs/promises');
const puppeteer = require('puppeteer');
const updateProgressFile = require('../utils/updateProgressFile');
const createFile = require('../utils/createFile');

const url = 'https://www.okaidi.fr';
const blockedRes = ['moment.5d51dd77.bundle.js', 'FAwaIww/uGC8', 'data:image/svg+xml', 'android-icon-144x144.png', 'Image-3-desk.png', 'pinterest.com', 'abtasty.com', 'go-mpulse.net', 'trustcommander.net', 'aticdn.net', 'tagcommander.com', 'verbolia.com', 'newrelic.com', 'scripts/Collector', 'jquery-ui.dragglable.min.js', 'avtag.min.js', 'lazysizes.min.js', 'google-analytics', '404_sq.jpg', 'fille.webp', '0494923_0.jpeg', '0494922_0.jpeg', '0494924_0.jpeg', '/0494925_0.jpeg', '0494926_0.jpeg', 'Visus@2x.jpg', '0495556_0.png', 'footer.bundle.js?v=4.78.13', 'ob-vp.webp', 'ok-vp.webp', 'produits.webp', 'privacy_v2_76.js'];

module.exports = class OkaidiService {
  puppeteerConfig;
  config;
  timeoutId;
  browser;
  page;
  outFileCSVPath;
  products;
  counter = 1;
  outFileJsonPath;
  outProducts = [];

  constructor(puppeteerConfig, config, products) {
    this.puppeteerConfig = puppeteerConfig;
    this.config = config;
    this.timeoutId = null;
    this.outFileCSVPath = `${BASE_OUT_CSV_FILES_PATH}/${config.outFile}.csv`;
    this.outFileJsonPath = `${BASE_OUT_CSV_FILES_PATH}/${config.outFile}.json`;
    this.products = products;
  }

  async init() {
    this.browser = await puppeteer.launch(this.puppeteerConfig);
    await this.browser.newPage();
    const pages = await this.browser.pages()
    this.page = pages[0];
    await this.page.setRequestInterception(true);

    this.page.on('request', (request) => {
      const url = request.url();
      const isInBlackList = blockedRes.some(domain => url.includes(domain));
      const isBlockedResource = ['stylesheet', 'media', 'texttrack', 'object', 'beacon', 'csp_report',].includes(request.resourceType());
      if (isBlockedResource || isInBlackList || url.endsWith('.gif') || url.endsWith('.svg') || url.endsWith('.ico')) request.abort();
      else request.continue();
    });

    await this.page.goto('https://www.okaidi.fr/t-shirt-love-a-manches-volantees-blanc-156678');
  }

  async scrape(inProduct) {
    try {
      if (!inProduct) { this.browser.close(); return null; }

      await this.page.waitForSelector('#okahead form');

      await this.page.evaluate((reference) => {
        const form = document.querySelector('#okahead form');
        form.querySelector('input').value = reference;
        form.submit();
      }, inProduct.reference);

      await this.page.waitForNavigation();
      await this.page.waitForSelector('#product-gallery');

      const product = await this.page.evaluate((inProduct) => {
        if(!document.querySelector('.product-name h1')) return inProduct;
        if (document.getElementById('related-products')) document.getElementById('related-products').style.display = 'none';
        return {
          ...inProduct,
          nom: document.querySelector('.product-name h1').textContent.trim(),
          images: [...document.querySelectorAll('.slick-track img')].map(v => v.src.replace('_100.jpg', '_800.jpg')).join('///'),
          description: document.querySelector('article .description-text').textContent.trim()
        }
      }, inProduct);

      return product;
    } catch (error) {
      console.log(colors.FgMagenta, '[Error] Scrape method ', error.message, colors.Reset);
      return inProduct;
    }
  }

  async run() {
    const { outFile, productsLen } = this.config;
    let counter = 0;
    console.log('> [Start setInterval]', counter);

    const handler = async () => {
      try {
        const isCanceled = await readFile(BASE_CANCEL_SCRAPE_FILE_PATH, { encoding: 'utf8' });
        if (counter >= productsLen || isCanceled === 'true') {
          this.browser.close();
          clearInterval(this.timeoutId);
          await createFile({ filename: outFile, data: this.outProducts.slice(0, -1), type: 'csv' });
          console.log('> [End setInterval]', counter);
          return;
        }
        else {
          counter++;
          const inProduct = this.products[counter];
          const response = await this.scrape(inProduct);
          this.outProducts = [...this.outProducts, response];
          response.done = response && (response.images || response.description) ? true : false;
          updateProgressFile({ outFile: this.config.outFile, inProduct: response, message: `[${response.done ? 'Done' : 'Not Found'}] Product: ${response.reference} (${counter}/${productsLen})` });
          await createFile({ filename: outFile, data: this.outProducts });
        }
      } catch (error) {
        this.browser.close();
        await updateProgressFile({ error: error.message, config: this.config, done: true });
        clearInterval(this.timeoutId);
        console.log('> (SetInterval Exit Error)', error);
      }
    }

    if (this.browser.isConnected()) this.timeoutId = setInterval(handler, this.config.delay * 1000);
  }
}