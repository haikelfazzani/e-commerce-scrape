const updateProgressFile = require('../utils/updateProgressFile');
const colors = require('../utils/colors');
const { BASE_CANCEL_SCRAPE_FILE_PATH, BASE_OUT_CSV_FILES_PATH, blocked_domains } = require('../utils/constants');
const formatRef = require('../utils/formatRef');
const { readFile, writeFile } = require('fs/promises');
const { json2csv } = require('json-2-csv');
const puppeteer = require('puppeteer');

const url = 'https://www.mayoral.com/fr/france';

module.exports = class MayoralService {
  puppeteerConfig;
  config;
  timeoutId;
  browser;
  page;
  outFileCSVPath;
  products;
  counter = 1;
  outFileJsonPath;
  outProducts = [];

  constructor(puppeteerConfig, config, products) {
    this.puppeteerConfig = puppeteerConfig;
    this.config = config;
    this.timeoutId = null;
    this.outFileCSVPath = `${BASE_OUT_CSV_FILES_PATH}/${config.outFile}.csv`;
    this.outFileJsonPath = `${BASE_OUT_CSV_FILES_PATH}/${config.outFile}.json`;
    this.products = products;
  }

  async init() {
    this.browser = await puppeteer.launch(this.puppeteerConfig);
    await this.browser.newPage();
    const pages = await this.browser.pages()
    this.page = pages[0];
    await this.page.setRequestInterception(true);
    await this.page.setViewport({ width: 453, height: 631 });
    // this.page.setJavaScriptEnabled(false);

    this.page.on('request', (request) => {
      const url = request.url();
      const isInBlackList = blocked_domains.some(domain => url.includes(domain)) || /(.svg|.webm|.mp4|.ttf|.woff2|.gif|.ico)$/gi.test(url);
      const isBlockedResource = ['stylesheet', 'media', 'font', 'texttrack', 'object', 'beacon', 'csp_report',].includes(request.resourceType());
      if (isBlockedResource || isInBlackList) request.abort();
      else request.continue();
    });

    await this.page.goto(url + '/search?searchTerm=00306-056');
  }

  async scrape(inProduct) {
    try {
      if (!inProduct) { this.browser.close(); clearInterval(this.timeoutId); return null; }

      inProduct.reference = formatRef(inProduct.reference);
      await this.page.goto(`${url}/search?searchTerm=${inProduct.reference}`, { waitUntil: 'domcontentloaded' });
      await this.page.waitForSelector('.MuiStack-root');

      const listItems = await this.page.$$eval('a[data-testid="product-card-name"]', li => li.length);
      if (listItems < 1) return inProduct;

      await this.page.evaluate(() => document.querySelector('a[data-testid="product-card-name"]').click());
      await this.page.waitForNavigation();

      await this.page.waitForSelector('#product-description', { visible: true });
      await this.page.waitForSelector('.MuiImageList-root.MuiImageList-quilted.scrollbar');

      const products = await this.page.evaluate(() => {
        // show sizes
        document.querySelector('button[aria-controls="size-selection-menu"]').click();

        const images = [...document.querySelectorAll('.MuiImageList-root.MuiImageList-quilted.scrollbar img')]
          .slice(0, 8)
          .map((el) => el.srcset);

        const tailles = [...document.querySelectorAll('li.MuiButtonBase-root[role="menuitem"]')]
          .map(v => v.textContent.trim())
          .filter(k => k)

        const couleur = document.querySelector('label.MuiFormControlLabel-root.MuiFormControlLabel-labelPlacementEnd.selected').textContent;

        return tailles.filter(v => v).map(t => {
          const taille = t.trim().replace(/\n|\t|\t\n/g, '').replace(/\D+/g, '');
          return {
            nom: document.querySelector('h1.MuiTypography-root.MuiTypography-h3').textContent.trim(),
            categorie: document.querySelectorAll('.MuiBreadcrumbs-ol li')[2].textContent.trim().split(' ')[0],
            description: document.querySelector('p.MuiTypography-root.MuiTypography-body1').textContent.trim(),
            images: images.join('///'),
            taille: taille,
            couleur: couleur.replace(/\s+|\n|\t|\t\n/g, '').trim(),
          }
        })
      });

      return products.map(p => {
        const ref = inProduct.reference.replace('-', '/');
        const reference = /ans/gi.test(p.taille) ? ref : ref + '-' + p.taille.replace(/Mois|ans/gi, '')
        return { ...inProduct, ...p, reference: reference.trim() }
      });
    } catch (error) {
      console.log(colors.FgMagenta, '[Error] ', error.message, colors.Reset);
      return inProduct;
    }
  }

  async run() {
    const { outFile, productsLen } = this.config;
    let counter = 0;
    console.log('> [Start setInterval]', counter);

    const handler = async () => {
      try {
        const isCanceled = await readFile(BASE_CANCEL_SCRAPE_FILE_PATH, { encoding: 'utf8' });
        if (counter >= productsLen || isCanceled === 'true') {
          this.browser.close();
          clearInterval(this.timeoutId);
          await createFile({ filename: outFile, data: this.outProducts.slice(0, -1), type: 'csv' });
          console.log('> [End setInterval]', counter);
          return;
        }
        else {
          counter++;
          const inProduct = this.products[counter];
          const response = await this.scrape(inProduct);
          this.outProducts = [...this.outProducts, response];
          response.done = response && (response.images || response.description) ? true : false;
          updateProgressFile({ outFile: this.config.outFile, inProduct: response, message: `[${response.done ? 'Done' : 'Not Found'}] Product: ${response.reference} (${counter}/${productsLen})` });
          await createFile({ filename: outFile, data: this.outProducts });
        }
      } catch (error) {
        this.browser.close();
        await updateProgressFile({ error: error.message, config: this.config, done: true });
        clearInterval(this.timeoutId);
        console.log('> (SetInterval Exit Error)', error);
      }
    }

    if (this.browser.isConnected()) this.timeoutId = setInterval(handler, this.config.delay * 1000);
  }
}
