const colors = require('../utils/colors');
const { BASE_CANCEL_SCRAPE_FILE_PATH, BASE_OUT_CSV_FILES_PATH } = require('../utils/constants');
const { readFile } = require('fs/promises');
const puppeteer = require('puppeteer');
const updateProgressFile = require('../utils/updateProgressFile');
const createFile = require('../utils/createFile');

const url = 'https://www.kiabi.com';

const blockedRes = ['kiabi_icon_pwa_144x144.png', 'flapJ12006.png', 'iovation-loader-5.x.js', 'assets/auto.js', 'xiti.com', 'data:img/png', 'data:image/gif', 'trustcommander.net', 'iadvize.com', 'appspot.com', 'desktop-home.webp', 'home-secondemain.webp', 'home-03-min.jpg', 'home-07-min.jpg', 'home-02-min.jpg', 'home-06-min.jpg'];

module.exports = class KiabiService {
  puppeteerConfig;
  config;
  timeoutId;
  browser;
  page;
  outFileCSVPath;
  products;
  counter = 1;
  outFileJsonPath;
  outProducts = [];

  constructor(puppeteerConfig, config, products) {
    this.puppeteerConfig = puppeteerConfig;
    this.config = config;
    this.timeoutId = null;
    this.outFileCSVPath = `${BASE_OUT_CSV_FILES_PATH}/${config.outFile}.csv`;
    this.outFileJsonPath = `${BASE_OUT_CSV_FILES_PATH}/${config.outFile}.json`;
    this.products = products;
  }

  async init() {
    this.browser = await puppeteer.launch(this.puppeteerConfig);
    await this.browser.newPage();
    const pages = await this.browser.pages()
    this.page = pages[0];
    await this.page.setRequestInterception(true);

    this.page.on('request', (request) => {
      const url = request.url();
      const isInBlackList = blockedRes.some(domain => url.includes(domain));
      const isBlockedResource = ['media', 'texttrack', 'object', 'beacon', 'csp_report',].includes(request.resourceType());
      if (isBlockedResource || isInBlackList || url.endsWith('.gif') || url.endsWith('.svg') || url.endsWith('.ico')) request.abort();
      else request.continue();
    });

    await this.page.goto(url);
  }

  async scrape(inProduct) {
    try {
      if (!inProduct || !inProduct.reference) { return null; }

      this.page.evaluate(() => {
        if (document.getElementById('popin_tc_privacy_button')) document.getElementById('popin_tc_privacy_button').click();
        if (document.querySelector('button[data-testid="CountryRedirectionDialog_dismissButton"]')) document.querySelector('button[data-testid="CountryRedirectionDialog_dismissButton"]').click()
        const btnReset = document.querySelector('div[class^="headerSearch_resetButton_"]');
        if (btnReset) btnReset.click();
      });

      const inputSelector = 'input[class^="headerSearch_searchInputInput_"]';
      await this.page.type(inputSelector, inProduct.reference);
      await this.page.click(inputSelector);
      await this.page.waitForSelector('div[class^="productList_listContainer_"]', { timeout: 5000 });

      const listItems = await this.page.evaluate((reference) => {
        const listItems = document.querySelectorAll('div[class^="productCard_productCardContainer_"] a');
        if (listItems.length > 0) {
          const isFound = [...listItems].find(item => (item.id || 'vvvvdcvv').toLowerCase().trim().includes(reference.toLowerCase().trim()));
          if (!isFound) return 0;
          else { isFound.click(); return 1; }
        }
        else return 0;
      }, inProduct.reference);

      if (listItems.length < 1) return inProduct;

      await this.page.waitForNavigation();
      await this.page.waitForSelector('div[class^="subMenu_bottomEndContentContainer_"]');

      const product = await this.page.evaluate((inProduct) => {
        const ref = document.querySelector('div[data-testid="productPage_div_productInfosRefContainer"]');
        if (!ref.textContent.includes(inProduct.reference)) return inProduct;
        
        const btnSideBar = document.querySelector('div[class^="subMenu_bottomEndContentContainer_"]');
        btnSideBar.click();
        const descEl1 = document.querySelector('div[data-testid="product_description_tab-content"]');
        const description = descEl1
          ? descEl1.textContent
          : document.querySelector('div[class^="productDescriptionTab_productDescriptionContent_"]').textContent;

        btnSideBar.click();

        return {
          ...inProduct,
          nom: document.querySelector('h1[data-testid="text"').textContent.trim(),
          images: [...document.querySelectorAll('img[data-testid="productPage_img_image"]')].map(v => v.src).join('///'),
          description,
        }
      }, inProduct);

      return product;
    } catch (error) {
      console.log(colors.FgMagenta, '[Error] Scrape method ', error.message, colors.Reset);
      return inProduct;
    }
  }

  async run() {
    const { outFile, productsLen } = this.config;
    let counter = 0;
    console.log('> [Start setInterval]', counter);

    const handler = async () => {
      try {
        const isCanceled = await readFile(BASE_CANCEL_SCRAPE_FILE_PATH, { encoding: 'utf8' });
        if (counter >= productsLen || isCanceled === 'true') {
          this.browser.close();
          clearInterval(this.timeoutId);
          await createFile({ filename: outFile, data: this.outProducts.slice(0, -1), type: 'csv' });
          console.log('> [End setInterval]', counter);
          return;
        }
        else {
          counter++;
          const inProduct = this.products[counter];
          const response = await this.scrape(inProduct);
          this.outProducts = [...this.outProducts, response];
          response.done = response && (response.images || response.description) ? true : false;
          updateProgressFile({ outFile: this.config.outFile, inProduct: response, message: `[${response.done ? 'Done' : 'Not Found'}] Product: ${response.reference} (${counter}/${productsLen})` });
          await createFile({ filename: outFile, data: this.outProducts });
        }
      } catch (error) {
        this.browser.close();
        await updateProgressFile({ error: error.message, config: this.config, done: true });
        clearInterval(this.timeoutId);
        console.log('> (SetInterval Exit Error)', error);
      }
    }

    if (this.browser.isConnected()) this.timeoutId = setInterval(handler, this.config.delay * 1000);
  }
}