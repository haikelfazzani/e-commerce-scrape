const colors = require('../utils/colors');
const { BASE_CANCEL_SCRAPE_FILE_PATH, BASE_OUT_CSV_FILES_PATH } = require('../utils/constants');
const { readFile } = require('fs/promises');
const puppeteer = require('puppeteer');
const updateProgressFile = require('../utils/updateProgressFile');
const createFile = require('../utils/createFile');

const url = 'https://www.toopty.net/index.php?fc=module&module=leoproductsearch&controller=productsearch';
const url_search_params = '&search_query=';

module.exports = class TooptyService {
  puppeteerConfig;
  config;
  timeoutId;
  browser;
  page;
  outFileCSVPath;
  products;
  counter = 1;
  outFileJsonPath;
  outProducts = [];

  constructor(puppeteerConfig, config, products) {
    this.puppeteerConfig = puppeteerConfig;
    this.config = config;
    this.timeoutId = null;
    this.outFileCSVPath = `${BASE_OUT_CSV_FILES_PATH}/${config.outFile}.csv`;
    this.outFileJsonPath = `${BASE_OUT_CSV_FILES_PATH}/${config.outFile}.json`;
    this.products = products;
  }

  async init() {
    this.browser = await puppeteer.launch(this.puppeteerConfig);
    await this.browser.newPage();
    const pages = await this.browser.pages();
    this.page = pages[0];
    await this.page.setRequestInterception(true);
    // await this.page.setJavaScriptEnabled(false);

    this.page.on('request', (request) => {
      const url = request.url();
      const isBlockedResource = ['stylesheet', 'media', 'font', 'texttrack', 'object', 'beacon', 'csp_report',].includes(request.resourceType());
      if (isBlockedResource || url.endsWith('.gif') || url.endsWith('.svg') || url.endsWith('.ico')) request.abort();
      else request.continue();
    });

    await this.page.goto(`${url}${url_search_params}${this.products[0].reference}`);
  }

  async scrape(inProduct) {
    try {
      if (!inProduct || !inProduct.reference) return null;
      await this.page.goto(`${url}${url_search_params}${inProduct.reference}`, { waitUntil: 'networkidle2', });
      await this.page.waitForSelector('#content-wrapper');

      const listItems = await this.page.$$('#js-product-list .ajax_block_product');
      if (listItems.length < 1) return inProduct;

      await this.page.evaluate(() => document.querySelector('#js-product-list .ajax_block_product a').click());
      await this.page.waitForNavigation();
      await this.page.waitForSelector('#thumb-gallery', { visible: true });

      const product = await this.page.evaluate(() => {
        return {
          images: [...document.querySelectorAll('#thumb-gallery .slick-track a')].map(v => v.dataset.image).join('///'),
          nom: document.querySelector('h1.product-detail-name').textContent.trim(),
          description: document.querySelector('.product-description').textContent.trim(),
        }
      });

      return { ...inProduct, ...product };
    } catch (error) {
      console.log(colors.FgMagenta, '[Error] Scrape method ', error.message, colors.Reset);
      return inProduct;
    }
  }

  async run() {
    const { outFile, productsLen } = this.config;
    let counter = 0;
    console.log('> [Start setInterval]', counter);

    const handler = async () => {
      try {
        const isCanceled = await readFile(BASE_CANCEL_SCRAPE_FILE_PATH, { encoding: 'utf8' });
        if (counter >= productsLen || isCanceled === 'true') {
          this.browser.close();
          clearInterval(this.timeoutId);
          await createFile({ filename: outFile, data: this.outProducts.slice(0, -1), type: 'csv' });
          console.log('> [End setInterval]', counter);
          return;
        }
        else {
          counter++;
          const inProduct = this.products[counter];
          const response = await this.scrape(inProduct);
          this.outProducts = [...this.outProducts, response];
          response.done = response && (response.images || response.description) ? true : false;
          updateProgressFile({ outFile: this.config.outFile, inProduct: response, message: `[${response.done ? 'Done' : 'Not Found'}] Product: ${response.reference} (${counter}/${productsLen})` });
          await createFile({ filename: outFile, data: this.outProducts });
        }
      } catch (error) {
        this.browser.close();
        await updateProgressFile({ error: error.message, config: this.config, done: true });
        clearInterval(this.timeoutId);
        console.log('> (SetInterval Exit Error)', error);
      }
    }

    if (this.browser.isConnected()) this.timeoutId = setInterval(handler, this.config.delay * 1000);
  }
}