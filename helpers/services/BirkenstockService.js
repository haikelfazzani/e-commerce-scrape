const colors = require('../utils/colors');
const { BASE_CANCEL_SCRAPE_FILE_PATH, BASE_OUT_CSV_FILES_PATH, blocked_domains } = require('../utils/constants');
const { readFile } = require('fs/promises');
const puppeteer = require('puppeteer');
const updateProgressFile = require('../utils/updateProgressFile');
const createFile = require('../utils/createFile');

const url = 'https://www.birkenstock.com/fr';
const url_search_params = '/search?lang=fr_FR&q=';

module.exports = class BirkenstockService {
  puppeteerConfig;
  config;
  timeoutId;
  browser;
  page;
  outFileCSVPath;
  products;
  counter = 1;
  outFileJsonPath;
  outProducts = [];

  constructor(puppeteerConfig, config, products) {
    this.puppeteerConfig = puppeteerConfig;
    this.config = config;
    this.timeoutId = null;
    this.outFileCSVPath = `${BASE_OUT_CSV_FILES_PATH}/${config.outFile}.csv`;
    this.outFileJsonPath = `${BASE_OUT_CSV_FILES_PATH}/${config.outFile}.json`;
    this.products = products;
  }

  async init() {
    this.browser = await puppeteer.launch(this.puppeteerConfig);
    await this.browser.newPage();
    const pages = await this.browser.pages()
    this.page = pages[0];
    await this.page.setRequestInterception(true);
    // await this.page.setJavaScriptEnabled(false);

    this.page.on('request', (request) => {
      const url = request.url();
      const isInBlackList = blocked_domains.some(domain => url.includes(domain));
      const isBlockedResource = ['stylesheet', 'media', 'font', 'texttrack', 'object', 'beacon', 'csp_report',].includes(request.resourceType());
      if (isBlockedResource || isInBlackList || url.endsWith('.gif') || url.endsWith('.svg') || url.endsWith('.ico')) request.abort();
      else request.continue();
    });

    this.page.goto(`${url}${url_search_params}${this.products[0].reference}`)
  }

  async scrape(inProduct) {
    try {
      if (!inProduct || !inProduct.reference) { return null; }
      await this.page.goto(`${url}${url_search_params}${inProduct.reference}`, { waitUntil: 'networkidle2', });
      await this.page.waitForSelector('#primary');

      const isFound = await this.page.evaluate(() => {
        const list = document.querySelectorAll('ul#search-result-items .xlt-producttile');
        if (!list || list.length < 1) return 0;

        const link = list.querySelector('a');
        if (link) { link.click(); return 1; }
        else return 0;
      });

      if (isFound < 1) return inProduct;
      await this.page.waitForNavigation();
      await this.page.waitForSelector('.slick-slide');

      const product = await this.page.evaluate(() => {
        const privacy = document.querySelector('.ui-dialog.ui-widget.ui-widget-content.ui-corner-all.privacy-overlay.no-close');
        if (privacy) privacy.style.display = 'none';
        return {
          nom: document.querySelector('.product-modelname-and-shortname').textContent.trim(),
          description: document.querySelector('.product-description-text').textContent.replace(/\s+|\n|\t|\t\n/g, ' ').trim(),
          images: [...document.querySelectorAll('.slick-slide picture source')].map(v => v.srcset).filter(v => v).join('///')
        }
      });

      return { ...inProduct, ...product };
    } catch (error) {
      console.log(colors.FgMagenta, '[Error] Scrape method ', error.message, colors.Reset);
      return inProduct;
    }
  }

  async run() {
    const { outFile, productsLen } = this.config;
    let counter = 0;
    console.log('> [Start setInterval]', counter);

    const handler = async () => {
      try {
        const isCanceled = await readFile(BASE_CANCEL_SCRAPE_FILE_PATH, { encoding: 'utf8' });
        if (counter >= productsLen || isCanceled === 'true') {
          this.browser.close();
          clearInterval(this.timeoutId);
          await createFile({ filename: outFile, data: this.outProducts.slice(0, -1), type: 'csv' });
          console.log('> [End setInterval]', counter);
          return;
        }
        else {
          counter++;
          const inProduct = this.products[counter];
          const response = await this.scrape(inProduct);
          this.outProducts = [...this.outProducts, response];
          response.done = response && (response.images || response.description) ? true : false;
          updateProgressFile({ outFile: this.config.outFile, inProduct: response, message: `[${response.done ? 'Done' : 'Not Found'}] Product: ${response.reference} (${counter}/${productsLen})` });
          await createFile({ filename: outFile, data: this.outProducts });
        }
      } catch (error) {
        this.browser.close();
        await updateProgressFile({ error: error.message, config: this.config, done: true });
        clearInterval(this.timeoutId);
        console.log('> (SetInterval Exit Error)', error);
      }
    }

    if (this.browser.isConnected()) this.timeoutId = setInterval(handler, this.config.delay * 1000);
  }
}