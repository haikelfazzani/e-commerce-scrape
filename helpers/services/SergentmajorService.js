const colors = require('../utils/colors');
const { BASE_CANCEL_SCRAPE_FILE_PATH, BASE_OUT_CSV_FILES_PATH } = require('../utils/constants');
const { readFile } = require('fs/promises');
const puppeteer = require('puppeteer');
const updateProgressFile = require('../utils/updateProgressFile');
const createFile = require('../utils/createFile');

const url = 'https://www.sergent-major.com';
const url_search_params = '/recherche?q=';

module.exports = class SergentmajorService {
  puppeteerConfig;
  config;
  timeoutId;
  browser;
  page;
  outFileCSVPath;
  products;
  counter = 1;
  outFileJsonPath;
  outProducts = [];

  constructor(puppeteerConfig, config, products) {
    this.puppeteerConfig = puppeteerConfig;
    this.config = config;
    this.timeoutId = null;
    this.outFileCSVPath = `${BASE_OUT_CSV_FILES_PATH}/${config.outFile}.csv`;
    this.outFileJsonPath = `${BASE_OUT_CSV_FILES_PATH}/${config.outFile}.json`;
    this.products = products;
  }

  async init() {
    this.browser = await puppeteer.launch(this.puppeteerConfig);
    await this.browser.newPage();
    const pages = await this.browser.pages()

    if (!this.page) {
      this.page = pages[0];
      this.page.setRequestInterception(true);
      this.page.setJavaScriptEnabled(false);

      this.page.on('request', (request) => {
        const url = request.url();
        const isBlockedResource = ['stylesheet', 'media', 'font', 'texttrack', 'object', 'beacon', 'csp_report',].includes(request.resourceType());
        if (isBlockedResource || url.endsWith('.gif') || url.endsWith('.svg') || url.endsWith('.ico')) request.abort();
        else request.continue();
      });

      await this.page.goto(url + url_search_params + '23E0AF61BNA321');
    }
  }

  async scrape(inProduct) {
    try {
      if (!inProduct) { this.browser.close(); return null; }
      let reference = inProduct.reference;
      if (inProduct.reference.includes('-')) inProduct.reference = inProduct.reference.split('-')[0]

      await this.page.goto(url + url_search_params + inProduct.reference, { waitUntil: 'domcontentloaded' });
      await this.page.waitForSelector('#product-search-results');

      const listProducts = await this.page.$$('.product-tile-container');
      if (listProducts.length < 1) return inProduct;

      const suggestions = await this.page.evaluate((reference) => {
        const isFound = [...document.querySelectorAll('.product-tile-container')].find((el) => {
          const child = el.querySelector('.product');
          if (!child) return null;
          return child.dataset.pid.includes(reference) ? el : null;
        });

        if (isFound) { isFound.querySelector("a.product-image-link").click(); return 1; }
        else return 0;
      }, inProduct.reference);

      await this.page.waitForNavigation();

      if (suggestions < 1) return inProduct;

      await this.page.waitForSelector('.product-slider-image', { visible: true });

      const product = await this.page.evaluate(() => {
        const description = document.querySelector('.product-description').textContent.trim();
        const descriptionli = [...document.querySelectorAll('#product-description li')].map(v => v.textContent.trim());

        return {
          nom: document.querySelector('.product-name').textContent.trim(),
          images: [...document.querySelectorAll('.product-slider-image')].slice(0, 8).map((el) => el.dataset.src || el.src).join('///'),
          description: description && description.length > 5 ? (description + '\n' + descriptionli.join('\n')) : descriptionli.join('\n')
        }
      });

      return { ...inProduct, ...product, reference };
    } catch (error) {
      console.log(colors.FgMagenta, '[Error] Scrape method ', error.message, colors.Reset);
      return inProduct;
    }
  }

  async run() {
    const { outFile, productsLen } = this.config;
    let counter = 0;
    console.log('> [Start setInterval]', counter);

    const handler = async () => {
      try {
        const isCanceled = await readFile(BASE_CANCEL_SCRAPE_FILE_PATH, { encoding: 'utf8' });
        if (counter >= productsLen || isCanceled === 'true') {
          this.browser.close();
          clearInterval(this.timeoutId);
          await createFile({ filename: outFile, data: this.outProducts.slice(0, -1), type: 'csv' });
          console.log('> [End setInterval]', counter);
          return;
        }
        else {
          counter++;
          const inProduct = this.products[counter];
          const response = await this.scrape(inProduct);
          this.outProducts = [...this.outProducts, response];
          response.done = response && (response.images || response.description) ? true : false;
          updateProgressFile({ outFile: this.config.outFile, inProduct: response, message: `[${response.done ? 'Done' : 'Not Found'}] Product: ${response.reference} (${counter}/${productsLen})` });
          await createFile({ filename: outFile, data: this.outProducts });
        }
      } catch (error) {
        this.browser.close();
        await updateProgressFile({ error: error.message, config: this.config, done: true });
        clearInterval(this.timeoutId);
        console.log('> (SetInterval Exit Error)', error);
      }
    }

    if (this.browser.isConnected()) this.timeoutId = setInterval(handler, this.config.delay * 1000);
  }
}