import Form from "@/components/Form";
import Wrapper from "@/components/Wrapper";

export default function settings() {
  return (
    <Wrapper>
      <div className="b-title">
        <h1 className='m-0'>Settings</h1>
        <p className="text-muted">Update application settings.</p>
      </div>

      <Form>
        <div>
          
        </div>
      </Form>
    </Wrapper>
  )
}
