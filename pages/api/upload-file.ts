import { BASE_IN_CSV_FILES_PATH } from '@/utils/constants';
import type { NextApiRequest, NextApiResponse } from 'next';
import formidable from 'formidable'
import { rename } from 'fs/promises';

export const config = {
  api: {
    bodyParser: false
  }
}

export default async function handler(request: NextApiRequest, res: NextApiResponse<any>) {
  const method = request.method;
  if (method === 'POST') {
    const form = new formidable.IncomingForm({ multiples: false, keepExtensions: true, uploadDir: BASE_IN_CSV_FILES_PATH });

    form.parse(request, (err, fields, files: formidable.Files) => {
      if (err) {
        console.error(err);
        res.status(500).json({ message: 'Error uploading file' });
        return;
      }

      rename(
        // @ts-ignore: Unreachable code error
        BASE_IN_CSV_FILES_PATH + '/' + files.file.newFilename,
        BASE_IN_CSV_FILES_PATH + '/' + fields.filename + '.xlsx'
      );

      return res.status(200).json({
        // @ts-ignore: Unreachable code error
        message:`"${fields.filename}.xlsx" File is uploaded successfully`,
        // @ts-ignore: Unreachable code error
        mimetype: files.file.mimetype,
        // @ts-ignore: Unreachable code error
        size: files.file.size,
        filename: fields.filename,
      });
    });
  }
}