import { BASE_CANCEL_SCRAPE_LOGS_FILE_PATH } from '@/utils/constants';
import { readFile } from 'fs/promises';
import type { NextApiRequest, NextApiResponse } from 'next';

export default async function handler(request: NextApiRequest, res: NextApiResponse<any>) {
  const method = request.method;
  if (method === 'GET') {
    
  }
  else {
    const content = await readFile(BASE_CANCEL_SCRAPE_LOGS_FILE_PATH, { encoding: 'utf8' });
    return res.status(200).json(content ? JSON.parse(content) : {});
  }
}