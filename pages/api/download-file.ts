import { BASE_OUT_CSV_FILES_PATH } from '@/utils/constants';
import { createReadStream } from 'fs';
import type { NextApiRequest, NextApiResponse } from 'next';
import path from 'path';

export default async function handler(request: NextApiRequest, res: NextApiResponse<any>) {
  const filePath = path.join(BASE_OUT_CSV_FILES_PATH, '' + request.query.file);
  res.setHeader('Content-Disposition', 'text/csv');
  res.setHeader('Content-Disposition', `attachment; filename="${request.query.file}"`);
  const stream = createReadStream(filePath);
  return stream.pipe(res);
}