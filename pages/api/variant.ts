import { BASE_IN_CSV_FILES_PATH, BASE_OUT_CSV_FILES_PATH, UPDATE_SET_VARIANT_WORKER } from '@/utils/constants';
import runWorkerPromise from '@/utils/runWorker';
import type { NextApiRequest, NextApiResponse } from 'next';
import { join } from 'path';

export default async function handler(request: NextApiRequest, res: NextApiResponse<any>) {
  const method = request.method;

  if (method === 'GET') {
    const { file, inDir, fileExt } = request.query;
    try {
      const products = await runWorkerPromise(UPDATE_SET_VARIANT_WORKER, {
        filePath: join((inDir === 'OUT' ? BASE_OUT_CSV_FILES_PATH : BASE_IN_CSV_FILES_PATH), '' + file),
        fileExtension:fileExt
      });
      return res.status(200).json(typeof products === 'string' ? null : products);
    } catch (error) {
      return res.status(400).json(null);
    }
  }
}