import ScrapeService from '@/services/ScrapeService';
import { FormScrape } from '@/types';
import { BASE_CANCEL_SCRAPE_FILE_PATH, BASE_OUT_CSV_FILES_PATH } from '@/utils/constants';
import { readFile, writeFile } from 'fs/promises';
import type { NextApiRequest, NextApiResponse } from 'next';

export default async function handler(request: NextApiRequest, res: NextApiResponse<any>) {
  const method = request.method;
  const { outFile, job } = request.query;

  if (method === 'GET' && job === 'cancel') {
    writeFile(BASE_CANCEL_SCRAPE_FILE_PATH, "true");
    res.status(200).json({ message: 'The scrape is cancelled' });
  }

  if (method === 'GET' && job === 'get-not-found-products-index') {
    const content: any = await readFile(`${BASE_OUT_CSV_FILES_PATH}/${outFile}.json`);
    const products = JSON.parse(content).map((k: any, i: number) => !k.images ? i + 1 : null).filter((v: any) => v);
    res.status(200).json(products);
  }

  if (method === 'POST' && job === 'scrape') {
    const data: FormScrape = JSON.parse(request.body);    
    ScrapeService.run(data);
    res.status(200).json(data);
  }
}