import type { NextApiRequest, NextApiResponse } from 'next';
import { readFile, unlink, writeFile } from 'fs/promises';
import path, { join } from 'path';
import { BASE_IN_CSV_FILES_PATH, BASE_OUT_CSV_FILES_PATH, READ_CSV_OR_XLSX_WORKER, READ_DIR_WORKER } from '@/utils/constants';
import runWorkerPromise from '@/utils/runWorker';
import { json2csv } from 'json-2-csv';
import { createReadStream, existsSync } from 'fs';

// get files
export default async function handler(request: NextApiRequest, res: NextApiResponse<any>) {
  const method = request.method;
  const { inDir, file, fileExt, type, job, service } = request.query;

  if (method === 'GET') {
    // show xlsx data from input directory '_in'
    if (job === 'fetch' && fileExt === 'xlsx') {
      const products: any[] = await runWorkerPromise<[]>(READ_CSV_OR_XLSX_WORKER, { isInFile: true, file, service });
      return res.status(200).json(products);
    }
    // show csv data from output directory '_out'
    if (job === 'fetch' && fileExt === 'csv') {
      const filePath = join(BASE_OUT_CSV_FILES_PATH, '' + file);
      const fileP = ('' + file).endsWith('.csv') ? filePath.replace('.csv', '.json') : filePath + '.json';
      const data = await readFile(fileP, { encoding: 'utf8' });
      const products = JSON.parse(data);
      return res.status(200).json(products);
    }

    // delete file
    if (job === 'delete' && type === 'file') {
      const filePath = inDir === 'OUT' ? join(BASE_OUT_CSV_FILES_PATH, '' + file) : join(BASE_IN_CSV_FILES_PATH, '' + file);
      unlink(filePath);
      const files: any = await runWorkerPromise(READ_DIR_WORKER, { inDir });
      return res.status(200).json({ message: (inDir === 'false' ? file : file) + ' File is deleted successfully', files });
    }

    // show directory files from '_in' or '_out'
    if (job === 'fetch' && type === 'dir') {
      const files: any = await runWorkerPromise(READ_DIR_WORKER, { inDir });
      return res.status(200).json(files);
    }

    if (job === 'download') {
      const filePath = path.join((inDir === 'OUT' ? BASE_OUT_CSV_FILES_PATH : BASE_IN_CSV_FILES_PATH), '' + request.query.file);
      res.setHeader('Content-Disposition', fileExt === 'xlsx' ? "application/vnd.ms-excel" : 'text/csv');
      res.setHeader('Content-Disposition', `attachment; filename="${request.query.file}"`);
      console.log(request.query.file,filePath);
      
      const stream = createReadStream(filePath);
      return stream.pipe(res);
    }
  }

  if (job === 'convert-json-to-csv' && type === 'file') {
    const { filename, products } = JSON.parse(request.body);
    const jsonFile = join(BASE_OUT_CSV_FILES_PATH, filename + '.json');
    if (!existsSync(jsonFile)) {
      writeFile(jsonFile, JSON.stringify(products), { encoding: 'utf8' });
      const cvn = await json2csv(products);
      writeFile(join(BASE_OUT_CSV_FILES_PATH, filename + '.csv'), cvn);
      return res.status(200).json(cvn);
    }
    else return res.status(200).json(products);
  }
}
