import Form from "@/components/Form";
import TableView from "@/components/TableView";
import Wrapper from "@/components/Wrapper";
import fetchJson from "@/utils/fetchJson";
import xlsxOrCsvToJSON from "@/utils/xlsxOrCsvToJSON";
import { ChangeEvent, useState } from "react";
import { toast, Toaster } from "sonner";

export default function page() {
  const [file, setFile] = useState<File>();
  const [isUploaded, setIsUploaded] = useState(true);
  const [products, setProducts]: any = useState([]);

  const onFileUploadChange = async (e: ChangeEvent<HTMLInputElement>) => {
    const file = e.target.files![0];
    const data = await xlsxOrCsvToJSON(file) as any[];
    setProducts(data);
    setFile(file);
  };

  const onSubmit = async (e: any) => {
    e.preventDefault();
    if (window.confirm('Do you really want to upload this file? ' + file?.name)) {
      setIsUploaded(false);

      const filename = e.target.elements[0].value;
      const formData = new FormData();
      formData.append("filename", filename);
      formData.append("file", file!);

      toast.promise(fetchJson("/api/upload-file", { method: "POST", body: formData, }), {
        loading: `Uploading File ${file?.name}...`,
        success: (json) => {
          setIsUploaded(true);
          return json.message;
        },
        error: 'Error',
      });

      e.target.reset();
    }
  }

  return (
    <Wrapper>
      <Toaster position="bottom-right" richColors theme="dark" visibleToasts={7} />

      <div className="b-title">
        <h1 className='m-0'>File upload</h1>
        <p className="text-muted">Only valid extension are CSV and XLSX.</p>
      </div>

      <Form onSubmit={onSubmit}>

        <div className="pb-2 border-bottom mb-2">
          <div className="d-flex flex-column mb-2">
            <label htmlFor="filename"><i className="fa fa-info-circle mr-1"></i>Filename</label>
            <small className="gray">Choose a name for your file (D'ont use spaces or special characters.)</small>
            <input className="w-100 mt-1" type="text" name="filename" id="filename" placeholder="mayoral" accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" required />
          </div>

          <div className="d-flex flex-column">
            <label htmlFor="file"><i className="fa fa-file mr-1"></i>File</label>
            <small>Choose a file</small>
            <input
              className="w-100 mt-1"
              name="file"
              type="file"
              onChange={onFileUploadChange}
              accept=".csv,.xlsx, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel"
              required
            />
          </div>
        </div>

        <div className="d-flex">
          <button className="w-100 btn mr-2" type="submit" disabled={!isUploaded}><i className="fa fa-upload mr-1"></i>upload file</button>
          <button className="w-100 btn bg-red" type="reset"><i className="fa fa-trash mr-1"></i>Reset</button>
        </div>
      </Form>

      <TableView products={products} file={file?.name || ''} options={{ contentEditable: true, setVariant: false, reset: false }} />
    </Wrapper>
  )
}
