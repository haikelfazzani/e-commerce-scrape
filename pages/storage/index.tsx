import Form from "@/components/Form";
import TableView from "@/components/TableView";
import Wrapper from "@/components/Wrapper";
import useFetch from "@/hooks/useFetch";
import fetchJson from "@/utils/fetchJson";
import { useEffect, useState } from "react";
import { toast, Toaster } from "sonner";

export default function index() {

  const [products, setProducts]: any = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [file, setFile] = useState<string>('');
  const [fileExt, setFileExt] = useState('xlsx')
  const [inDir, setInDir] = useState('IN');
  const [inputFiles, setInputFiles]: any = useFetch([], '/api/files?inDir=IN&job=fetch&type=dir');

  useEffect(() => {
    if (inputFiles && inputFiles.length > 0) {
      setFile(inputFiles[0]);
      setFileExt((inputFiles[0].match(/\.([^.]*?)(?=\?|#|$)/) || [])[1]);
    }
  }, [inputFiles]);

  const onChange = async (e: any) => {
    const val = e.target.value;
    if (e.target.name === 'file') {
      setFileExt((val.match(/\.([^.]*?)(?=\?|#|$)/) || [])[1]);
      setFile(val);
    }
    else {
      const files = await fetchJson(`/api/files?inDir=${val}&job=fetch&type=dir`);
      setInputFiles(files);
      setInDir(val);
      setFileExt((files[0].match(/\.([^.]*?)(?=\?|#|$)/) || [])[1]);
      setFile(files[0]);
    }
  }

  const onSubmit = async (e: any) => {
    e.preventDefault();
    setIsLoading(true);

    toast.promise(fetchJson(`/api/files?file=${file}&type=file&fileExt=${fileExt}&job=fetch`), {
      loading: 'Loading Products...',
      success: (products) => {
        setProducts(products);
        setIsLoading(false);
        return `${products.length} Products has been loaded`;
      },
      error: (error: any) => {
        setIsLoading(false);
        return error.message
      },
    });
  }

  const onDelete = async () => {
    if (window.confirm('Do you really want to delete this file?')) {
      setIsLoading(true);
      toast.promise(fetchJson(`/api/files?inDir=${inDir}&job=delete&type=file&file=${file}`), {
        loading: 'Deleting File...',
        success: (response) => {
          setProducts([]);
          setFile(response.files[0]);
          setInputFiles(response.files);
          setIsLoading(false);
          return response.message;
        },
        error: (error: any) => {
          setIsLoading(false);
          return error.message
        },
      });
    }
  }

  return <Wrapper>
    <Toaster position="bottom-right" richColors theme="dark" visibleToasts={7} />

    <div className="b-title">
      <h1 className='m-0'>Storage</h1>
      <p className="text-muted">All input and ouput files.</p>
    </div>

    <Form onSubmit={onSubmit}>
      <div className="d-flex mb-2 pb-2 border-bottom">
        <div className="w-100 d-flex flex-column mr-1">
          <label htmlFor="file"><i className="fa fa-file mr-1"></i>File</label>
          <small>Choose a file</small>
          <select className="w-100 mt-1" name="file" id="file" onChange={onChange} value={file} required>
            {inputFiles.map((f: string, i: number) => <option value={f} key={i}>{f}</option>)}
          </select>
        </div>

        <div className="w-100 d-flex flex-column ml-1">
          <label htmlFor="directory"><i className="fa fa-folder mr-1"></i>Directory</label>
          <small>Choose a directory</small>
          <select className="w-100 mt-1" name="directory" id="directory" onChange={onChange} value={inDir} required>
            {['IN', 'OUT'].map((f: string, i: number) => <option value={f} key={i}>_{f}</option>)}
          </select>
        </div>
      </div>

      <div className="d-flex">
        <button className='w-100 btn mr-1' type='submit' disabled={isLoading || inputFiles.length < 1}>
          <i className="fa fa-eye mr-1"></i>Show data ({file})
        </button>
        <button className='w-100 btn bg-red ml-1' type='button' onClick={onDelete} disabled={isLoading || inputFiles.length < 1}>
          <i className="fa fa-trash mr-1"></i>delete file ({file})
        </button>
      </div>
    </Form>

    <TableView fileExt={fileExt} inDir={inDir} products={products} file={file} />
  </Wrapper>
}
