import { isScrapingCanceledAtom, progressAtom } from "@/atoms";
import fetchJson from "@/utils/fetchJson";
import { useAtom } from "jotai";
import { useEffect } from "react";

let id: any;
export default function useScrape() {
  const [_, setProgress]: any = useAtom(progressAtom);
  const [isScrapingCanceled, setIsScrapingCanceled] = useAtom(isScrapingCanceledAtom);

  useEffect(() => {
    const handler = async () => {
      try {
        const data = await fetchJson('/api/progress', { method: 'POST' });

        if (data && data.products.length > 0 && data.columns.length < 1) {
          const columns = Object.keys(data.products.find((p: any) => p.images || p.description) || data.products[0]);
          setProgress({ ...data, columns });
        }
        else setProgress(data)

        if (data.done) {
          const data = await fetchJson('/api/progress', { method: 'POST' });
          if (data && data.products.length > 0 && data.columns.length < 1) {
            const columns = Object.keys(data.products.find((p: any) => p.images || p.description) || data.products[0]);
            setProgress({ ...data, columns });
          }
          else {
            setProgress(data)
            setIsScrapingCanceled(!isScrapingCanceled);
          }
          clearInterval(id);
        }
      } catch (error) {
        console.log('Use scrape error ==> ', error);
        clearInterval(id);
      }
    }

    id = setInterval(handler, 3000);

    return () => { clearInterval(id); }
  }, []);
}