import { isScrapingCanceledAtom } from '@/atoms';
import { useAtom } from 'jotai';

export default function BtnCancelScrape() {
  const [_, setIsScrapingCanceled] = useAtom(isScrapingCanceledAtom);

  const onCancelScrape = async () => {
    if (window.confirm('Do you really want to cancel scrape?')) {
      setIsScrapingCanceled(true);
      await fetch('/api/scrape?job=cancel');
    }
  }

  return (
    <button className="btn bg-inherit red br-0 p-0" onClick={onCancelScrape}><i className="fa fa-ban mr-1"></i>cancel scrape</button>
  )
}
