import { progressAtom } from "@/atoms";
import { useAtom } from "jotai";
import { useEffect, useState } from "react";

const formatSeconds = (seconds: number) => {
  let date = new Date(seconds * 1000);
  let hh: any = date.getUTCHours();
  let mm: any = date.getUTCMinutes();
  let ss: any = date.getSeconds();
  if (hh < 10) { hh = "0" + hh; };
  if (mm < 10) { mm = "0" + mm; };
  if (ss < 10) { ss = "0" + ss; };
  return hh + ":" + mm + ":" + ss;
}

let id: any;
const time = new Date().toISOString().slice(11, 19);

export default function Timer() {
  const [progress]: any = useAtom(progressAtom);
  const [counter, setCounter] = useState(0);
  const [endTime, setEndTime] = useState(time);
  const [isStoped, setIsStoped] = useState(false)

  useEffect(() => {
    if (progress.done || isStoped) { clearInterval(id); return; }

    id = setInterval(() => {
      setCounter(c => c + 1);
      setEndTime(new Date().toISOString().slice(11, 19));
    }, 1000);

    return () => {
      clearInterval(id)
    }
  }, [progress.done, isStoped]);

  const onStop = () => {
    clearInterval(id);
    setIsStoped(true)
  }

  const onPlay = () => {
    if(!progress.done) setIsStoped(false)
  }

  return (
    <div className="w-100 bg-dark shadow d-flex justify-between align-center uppercase br7 mb-2">
      <small className="p-1 d-flex align-center"><i className="fa fa-clock mr-1"></i>Time infos</small>
      <small className="p-1 d-flex align-center"><i className="fa fa-stopwatch mr-1"></i>Timer: {formatSeconds(counter)}</small>
      <small className="p-1 d-flex align-center"><i className="fa fa-clock mr-1"></i>Start time: {time}</small>
      <small className="p-1 d-flex align-center"><i className="fa fa-clock mr-1"></i>End time: {endTime}</small>
      {isStoped
        ? <button className="btn bg-green border-0 border-left br-0" onClick={onPlay}><i className="fa fa-play"></i></button>
        : <button className="btn bg-red border-0 border-left br-0" onClick={onStop}><i className="fa fa-stop"></i></button>}
    </div>
  )
}
