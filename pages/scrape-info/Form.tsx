import { formScrapeAtom } from "@/atoms";
import { FormScrape } from "@/types";
import { useAtom } from "jotai";

export default function Form() {
  const [state] = useAtom<FormScrape>(formScrapeAtom);

  return <ul className="bg-dark shadow br7 mb-3" style={{display:'grid', gridTemplateColumns:'1fr 1fr 1fr 1fr 1fr 1fr 1fr',gap:'10px'}}>
    <li className="d-flex flex-column border-right p-1"><small className="uppercase">Service</small><span className="mt-1">{state.service}</span></li>
    <li className="d-flex flex-column border-right p-1"><small className="uppercase">In File</small><span className="mt-1">{state.inFile}</span></li>
    <li className="d-flex flex-column border-right p-1"><small className="uppercase">Out File</small><span className="mt-1">{state.outFile + '.csv'}</span></li>
    <li className="d-flex flex-column border-right p-1"><small className="uppercase">Delay</small><span className="mt-1">{state.delay}</span></li>
    <li className="d-flex flex-column border-right p-1"><small className="uppercase">Headless</small><span className="mt-1">{state.headless.toString()}</span></li>
    <li className="d-flex flex-column border-right p-1"><small className="uppercase">limit From</small><span className="mt-1">{state.limitFrom}</span></li>
    <li className="d-flex flex-column p-1"><small className="uppercase">limit To</small><span className="mt-1">{state.limitTo}</span></li>
  </ul>
}
