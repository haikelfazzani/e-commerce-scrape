import { useAtom } from "jotai";
import BtnCancelScrape from "./BtnCancelScrape";
import { progressAtom } from "@/atoms";
import jsonToCSVorXLSX from "@/utils/jsonToCSVorXLSX";
import styles from "@/styles/Table.module.css";

export default function Logs() {
  const [data] = useAtom(progressAtom);
  if (!data || !data.products) return <></>;

  const onDownload = (e: any) => {
    e.preventDefault();
    const filename = e.target.elements[0].value;
    const fileType = e.target.elements[1].value;
    const products = data.products.map((v: any) => { delete v.done; return v; });
    jsonToCSVorXLSX(products, filename, fileType);
  }

  return <div className='w-100'>
    <div className="w-100 bg-dark shadow mb-2 br7">
      <p className={"p-2 d-flex justify-between align-center " + (data.message.includes('scrape ended') ? 'red':'green')}>
        <span className="translateX"><i className="fa fa-arrow-right mr-1"></i>{data.message}</span>
        <BtnCancelScrape />
      </p>

      {data.done && <form onSubmit={onDownload} className="p-2 d-flex justify-between align-center border-top">
        <input className="w-100 mr-1" type="text" name="filename" id="filename" defaultValue={data.outFile} placeholder="filename" required />
        <select className="w-100 mr-1" name="fileType" id="fileType" required>
          <option value="csv">csv</option>
          <option value="xlsx">Excel</option>
        </select>
        <button type="submit" className="btn"><i className="fa fa-download mr-1"></i>download</button>
      </form>}
    </div>

    {data.products.length > 0 && <div className={'w-100 ' + styles.table} style={{ maxHeight: '500px' }}>
      <table className='w-100 overflow br7'>
        <thead>
          <tr>
            <th>#</th>
            {data.columns.map((p: any, i: number) => <th key={i} style={{ width: p === 'reference' || p === 'variant' ? '150px' : '50px' }}>{p}</th>)}
          </tr>
        </thead>
        <tbody>
          {data.products.filter((v: any) => v).map((p: any, i: number) => <tr key={i + 'id'} className={p.done ? "bg-green cp" : "bg-red cp"}>
            <td>{i + 1}</td>
            {data.columns.map((k: string, indx: number) => <td className="scale" key={indx}>
              {p && p[k] ? p[k].toString() : ''}
            </td>)}
          </tr>)}
        </tbody>
      </table>
    </div>}
  </div>
}