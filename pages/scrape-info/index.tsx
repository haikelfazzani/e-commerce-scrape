import Logs from "./Logs";
import Form from "./Form";
import useScrape from "./useScrape";
import Wrapper from "@/components/Wrapper";

export default function Page() {
  useScrape();

  return <Wrapper>
    <div className="b-title">
      <h1 className='m-0'>Scrape Result</h1>
      <p className="text-muted">D'ont change page, be patient until scrape complete.</p>
    </div>

    <Form />
    <Logs />
  </Wrapper>
}
