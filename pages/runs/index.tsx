import { formScrapeAtom, progressAtom, scrapeHistoryAtom } from "@/atoms";
import Form from "@/components/Form";
import Wrapper from "@/components/Wrapper";
import useFetch from "@/hooks/useFetch";
import { services } from "@/utils/constants";
import fetchJson from "@/utils/fetchJson";
import { useAtom } from "jotai";
import { useRouter } from "next/router";

export default function Home() {
  const router = useRouter();
  const [progress, setProgress]: any = useAtom(progressAtom);
  const [scrapeHistory, setScrapeHistory]: any = useAtom(scrapeHistoryAtom);
  const [state, setState] = useAtom(formScrapeAtom);
  const [files]: any = useFetch([], '/api/files?inDir=IN&&job=fetch&type=dir');

  const onchange = (e: any) => {
    if (e && e.target) {
      const value = e.target.name === "headless" ? JSON.parse(e.target.value) : e.target.value;
      setState({ ...state, [e.target.name]: value });
    }
  }

  const onSubmit = async (e: any) => {
    e.preventDefault();

    if (window.confirm('Do you really want to scrape ' + state.service)) {
      const response = await fetchJson('/api/scrape?job=scrape', { method: 'POST', body: JSON.stringify(state) });
      if (response) {
        setState(state);
        setProgress({ message: null, messages: null, outFile: '', done: false, color: 'black' });

        const date = new Date().toISOString().split('T');
        setScrapeHistory([...scrapeHistory, { ...state, date: date[0], time: date[1] }])
        router.push('/scrape-info');
      }
    }
  }

  return (
    <Wrapper>
      <div className="b-title">
        <h1 className='m-0'>Runs</h1>
        <p className="text-muted">Space of scrape</p>
      </div>

      <Form className="shadow br7 p-2" onSubmit={onSubmit}>

        <div className="grid-2 pb-3 border-bottom mb-2">
          <div>
            <div className="d-flex flex-column mb-2">
              <label htmlFor="service"><i className="fa fa-globe mr-1"></i>Service</label>
              <small>Choose a service</small>
              <select className="w-100 mt-1" name="service" id="service" onChange={onchange} value={state.service}>
                {services.map(v => <option key={v.value} value={v.value}>{v.value}</option>)}
              </select>
            </div>

            <div className="d-flex flex-column mb-2">
              <label htmlFor="inFile"><i className="fa fa-file mr-1"></i>Input file</label>
              <small>Choose an input file</small>
              <select className="w-100 mt-1" name="inFile" id="inFile" onChange={onchange} value={state.inFile}>
                {files.map((v: any) => <option key={v} value={v}>{v}</option>)}
              </select>
            </div>

            <div className="w-100 d-flex flex-column mt-2">
              <label htmlFor="outFile"><i className="fa fa-file mr-1"></i>Output File</label>
              <small>Choose a name for your output file. (Don't use spaces and special characters)</small>
              <input className='w-100 mt-1' type="text" name="outFile" id="outFile" onChange={onchange} value={state.outFile} placeholder='mayoral' required />
            </div>
          </div>

          <div>
            <div className='mr-1 d-flex flex-column mb-2'>
              <label htmlFor="headless"><i className="fa fa-tablet mr-1"></i>headless</label>
              <small className="light">headless meaning without interface</small>
              <select className='w-100 mt-1' title="Headless" name="headless" id="headless" onChange={onchange} value={'' + state.headless} required>
                <option value="true">true</option>
                <option value="false">false</option>
              </select>
            </div>

            <div className='mr-1 d-flex flex-column mb-2'>
              <label htmlFor="delay"><i className="fa fa-clock mr-1"></i>Delay</label>
              <small className="light">Scrape time between each product</small>
              <input className='w-100 mt-1' title="Delay" type="number" name="delay" id="delay" min={2} max={300} onChange={onchange} value={state.delay} required />
            </div>

            <div className='d-flex flex-column'>
              <label htmlFor="limitFrom" title="Limit Length of Products To Be Scraped">
                <i className="fa fa-sort-numeric-down mr-1"></i>Limit "from" "to" of Products To Be Scraped
              </label>
              <small className="light">from 0 to 0 meaning all products. (first product start from 0)</small>
              <div className='d-flex mt-1'>
                <input className="w-100 mr-1" title="From" min={0} type="number" name="limitFrom" id="limitFrom" onChange={onchange} value={state.limitFrom} required />
                <input className="w-100 ml-1" title="To" min={0} type="number" name="limitTo" id="limitTo" onChange={onchange} value={state.limitTo} required />
              </div>
            </div>
          </div>
        </div>

        <div className="d-flex align-center">
          <button className='w-100 btn mr-2' type='submit'><i className="fa fa-play mr-1"></i>Start scrape ({state.service})</button>
          <button className="w-100 btn bg-red" type="reset"><i className="fa fa-trash mr-1"></i>Reset</button>
        </div>
      </Form>
    </Wrapper>
  )
}
