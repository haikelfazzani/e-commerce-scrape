import { scrapeHistoryAtom } from "@/atoms";
import Wrapper from "@/components/Wrapper";
import { useAtom } from "jotai";

export default function history() {
  const [scrapeHistory, setScrapeHistory]: any = useAtom(scrapeHistoryAtom);

  const onClear = () => {
    if (window.confirm('Do you really want to clear history?')) setScrapeHistory([])
  }

  return (
    <Wrapper>
      <div className="b-title">
        <h1 className='m-0'>History</h1>
        <p className="text-muted">all scrape history.</p>
      </div>

      <div className="bg-dark shadow br7">
        <ul>
          <li className="w-100 border-bottom grid-5 uppercase">
            <small className="p-1 border-right">Service</small>
            <small className="p-1 border-right">Input File</small>
            <small className="p-1 border-right">Output File</small>
            <small className="p-1 border-right">Date</small>
            <small className="p-1">Time</small>
          </li>
        </ul>

        {scrapeHistory && scrapeHistory.length > 0 && <ul>
          {scrapeHistory.map((h: any, i: number) => <li className="w-100 border-bottom grid-5 overflow" key={i} style={{ maxHeight: '500px' }}>
            <span className="p-1 border-right">{h.service}</span>
            <span className="p-1 border-right">{h.inFile}</span>
            <span className="p-1 border-right">{h.outFile}.csv</span>
            <span className="p-1 border-right">{new Date(h.date).toDateString()}</span>
            <span className="p-1 border-right">{h.time}</span>
          </li>)}
        </ul>}
      </div>

      {scrapeHistory && scrapeHistory.length > 0 && <button className="btn bg-red mt-3" onClick={onClear}>
        <i className="fa fa-trash mr-1"></i>Clear history</button>}
    </Wrapper>
  )
}
