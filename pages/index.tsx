import Wrapper from "@/components/Wrapper";
import Link from "next/link";

export default function index() {
  return (
    <Wrapper>

      <ul className="grid-3 mt-3">
        <Link href="/account" className="card bg-dark p-3 center shadow br7">
          <div>
            <h1 className="m-0"><i className="fa fa-user display-1"></i></h1>
            <h1 className="mb-0">Account</h1>
          </div>
        </Link>

        <Link href="/runs" className="card bg-dark p-3 center shadow br7">
          <div>
            <h1 className="m-0"><i className="fa fa-play display-1"></i></h1>
            <h1 className="mb-0">Runs</h1>
          </div>
        </Link>

        <Link href="/storage" className="card bg-dark p-3 center shadow br7">
          <div>
            <h1 className="m-0"><i className="fa fa-database display-1"></i></h1>
            <h1 className="mb-0">Storage</h1>
          </div>
        </Link>

        <Link href="/upload-file" className="card bg-dark p-3 center shadow br7">
          <div>
            <h1 className="m-0"><i className="fa fa-file display-1"></i></h1>
            <h1 className="mb-0">Upload file</h1>
          </div>
        </Link>

        <Link href="/history" className="card bg-dark p-3 center shadow br7">
          <div>
            <h1 className="m-0"><i className="fa fa-clock display-1"></i></h1>
            <h1 className="mb-0">History</h1>
          </div>
        </Link>

        <Link href="/settings" className="card bg-dark p-3 center shadow br7">
          <div>
            <h1 className="m-0"><i className="fa fa-cog display-1"></i></h1>
            <h1 className="mb-0">Settings</h1>
          </div>
        </Link>
      </ul>
    </Wrapper>
  )
}
