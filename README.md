# Web application for scraping e-commerce based on excel or csv

# Build project and run server locally
```shell
# only one time
bash run.sh
# and open URL in browser
http://localhost:3000
```

# dev
```shell
npm run dev
# and open URL in browser
http://localhost:3000
```

### Notes
```js
// Minimum excel or csv columns
Columns =  ['reference']
```

# Todo
- [x] sergent-major.com
- [x] dpam.com
- [x] birkenstock.com
- [x] toopty.net
- [x] mayoral.com
- [x] kiabi.com
- [x] citymode.com
- [x] petit-bateau.tn
- [x] gimel-tunisie.com
- [x] gioseppotunisie.com
- [x] petit-bateau.fr
- [x] okaidi.fr
- [ ] ovsfashion.com
- [ ] chicco.fr

# Captures
![](captures/capture.png)
![](captures/capture2.png)
![](captures/capture3.png)
![](captures/capture4.png)
![](captures/capture5.png)