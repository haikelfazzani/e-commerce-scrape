import puppeteer, { Browser, Page } from 'puppeteer';
import { inProduct, PuppeteerConfig } from "../types/index";

const url_tn = 'https://www.petit-bateau.tn';

export default class PetitBateauService {
  config: PuppeteerConfig;
  timeoutId: any;
  browser!: Browser;
  page!: Page;
  products!: inProduct[];
  counter: number = 1;
  outProducts: any = [];

  constructor(config: PuppeteerConfig, products: inProduct[]) {
    this.config = config;
    this.timeoutId = null;
    this.products = products;
  }

  async init() {
    this.browser = await puppeteer.launch(this.config);
    await this.browser.newPage();
    const pages = await this.browser.pages()
    this.page = pages[0];
    await this.page.setRequestInterception(true);
    this.page.setJavaScriptEnabled(false);
  }

  private async scrape(inProduct: any) {
    // petit bateau tunisie
    await this.page.goto(url_tn + '/instantsearch/result/?q=' + inProduct.reference, { waitUntil: 'domcontentloaded', });
    await this.page.waitForSelector('#maincontent');

    const listProducts = await this.page.$$('.product-tile-container');
    if (listProducts.length < 1) return 0;

    console.log(listProducts[0].evaluate(v => v.textContent));

    this.browser.isConnected()

    // this.page.waitForNavigation({timeout: timeout, waitUntil: ['domcontentloaded', waitUntil]})
    const divCount = await this.page.$$eval('ol.products.list.items.product-items li', divs => divs.length);
    const pHandler = await this.page.$$('ol.products.list.items.product-items li');
    // pHandler[0].
    await this.page.evaluateHandle(list => list[0].$eval('a', el => el.click()), pHandler);

    await this.page.$eval('ol.products.list.items.product-items li a', divs => divs.click());
    this.page.evaluate(()=>{})

    this.page.evaluateHandle(()=>{});
    this.page.goBack()

    await this.page.waitForNavigation({timeout:1000});
    new Promise(r => setTimeout(r, 100));
  }
}
