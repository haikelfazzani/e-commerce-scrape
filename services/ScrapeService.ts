import { FormScrape, inProduct } from "@/types";
import config from '../utils/puppeteer';
import { UPDATE_PROGRESS_WORKER, READ_CSV_OR_XLSX_WORKER, BASE_CANCEL_SCRAPE_FILE_PATH, SERVICE_BUILDER_WORKER, BASE_CANCEL_SCRAPE_LOGS_FILE_PATH } from "@/utils/constants";
import { isMainThread } from "worker_threads";
import runWorker from "@/utils/runWorker";
import { writeFile } from "fs/promises";
import sleep from "@/utils/sleep";
import { writeFileSync } from "fs";

export default class ScrapeService {
  static async run(request: FormScrape) {
    try {
      await writeFile(BASE_CANCEL_SCRAPE_LOGS_FILE_PATH, JSON.stringify({ message: '[Scrape Started] Please be patient', done: false, products: [], columns: [], outFile: '' }));
      await writeFile(BASE_CANCEL_SCRAPE_FILE_PATH, "false");

      let products: inProduct[] = [];
      const data: any[] = await runWorker<[]>(READ_CSV_OR_XLSX_WORKER, { isInFile: true, file: request.inFile, service: request.service });
      data.unshift({});

      products = data.slice(
        +request.limitFrom > 0 ? +request.limitFrom : 0,
        +request.limitTo > 0 ? +request.limitTo + 1 : data.length
      );

      const productsLen = products.length - 1;
      runWorker(UPDATE_PROGRESS_WORKER, { config: request, productsLen });
      await sleep(1000);
      writeFileSync(BASE_CANCEL_SCRAPE_LOGS_FILE_PATH, JSON.stringify({ message: `[In File] ${request.inFile} (${productsLen}°items)`, done: false, products: [], columns: [], outFile: '' }));

      if (isMainThread && products.length > 0) {
        await sleep(2000);

        await runWorker(SERVICE_BUILDER_WORKER, {
          service: request.service,
          puppeteerConfig: {
            ...config,
            headless: request.headless,
          },
          config: {
            ...request,
            productsLen
          },
          products
        });
      }
      else {
        await writeFile(BASE_CANCEL_SCRAPE_LOGS_FILE_PATH, JSON.stringify({ message: '[Worker Ended] scrape ended (0°items)', done: false, products: [], columns: [], outFile: '' }));
      }
    } catch (error: any) {
      runWorker(UPDATE_PROGRESS_WORKER, { message: '[Worker Ended] scrape ended', done: true });
      console.log(error);
    }
  }
}