const WEATHER_API_URL = 'https://api.weatherapi.com/v1/forecast.json?key=b44cb5395a644ba08fd10946202108&q=';

export default class WeatherService {

  static async getData(city = 'Africa/Tunis') {
    try {
      const isToday = this.getLocalData() && new Date(this.getLocalData().current.last_updated).toLocaleDateString() === new Date().toLocaleDateString();

      if (isToday) {
        return this.getLocalData();
      }
      else {
        city = city.split('/')[1].toLowerCase();

        let items = await fetch(WEATHER_API_URL + city);
        items = await items.json();

        localStorage.setItem('weather', JSON.stringify(items));

        return items;
      }
    } catch (error) {
      return this.getLocalData();
    }
  }

  static getLocalData() {
    let localData = localStorage.getItem('weather');
    return localData ? JSON.parse(localData) : null;
  }
}