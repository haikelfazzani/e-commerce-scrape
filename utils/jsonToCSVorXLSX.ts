import { write, utils } from "xlsx";
import { json2csv } from 'json-2-csv';

function jsonToXLSX(jsonArray: any[]) {
  const workbook = utils.book_new();
  const worksheet = utils.json_to_sheet(jsonArray);
  utils.book_append_sheet(workbook, worksheet, "Sheet1");
  return write(workbook, { bookType: "xlsx", type: "array" });
}

export default async function jsonToCSVorXLSX(data: any[], fileName: string, fileType = 'csv') {
  const result = fileType === 'xlsx' ? jsonToXLSX(data) : await json2csv(data);
  const blob = new Blob([result], { type: fileType === 'xlsx' ? "application/octet-stream" : "text/csv" });

  const downloadLink = document.createElement("a");
  downloadLink.href = URL.createObjectURL(blob);
  downloadLink.download = fileName + (fileType === 'xlsx' ? ".xlsx" : ".csv");

  document.body.appendChild(downloadLink);
  downloadLink.click();
  downloadLink.remove()
}
