import { read, utils } from "xlsx";
import * as Papa from 'papaparse';

export default function xlsxOrCsvToJSON(file: any): Promise<[]> {
  let result: any = [];

  return new Promise((resolve) => {
    try {
      const reader = new FileReader();
      reader.onload = function (e: any) {
        const content = e.target.result;

        if (file.name.endsWith('.xlsx')) {
          const data = new Uint8Array(content);
          const workbook = read(data, { type: "array" });
          const worksheet = workbook.Sheets[workbook.SheetNames[0]];
          const jsonData = utils.sheet_to_json(worksheet, { header: 1 });
          const headers = jsonData.shift() as string[];

          result = jsonData.filter((v: any) => v.length > 0).map((row: any) =>
            headers.reduce((obj: any, header: string, index: number) => {
              if (row && row.length > 0) {
                obj[header] = row[index];
                return obj;
              }
            }, {})
          );
        }
        else {
          const parsed = Papa.parse(content);

          result = parsed.data.slice(1).map((row: any) => {
            const obj: any = {};
            (parsed.data[0] as string[]).forEach((column: string, index: number) => {
              obj[column] = /reference/gi.test(column) ? '' + row[index] : row[index];
            });
            return obj;
          });
        }

        resolve(result);
      };

      reader[file.name.endsWith('.xlsx') ? 'readAsArrayBuffer' : 'readAsText'](file);
    } catch (error) {
      return result
    }
  });
}