import { join } from "path";
import { PuppeteerLaunchOptions } from "puppeteer";

const cacheDirectory = join(process.cwd(), '.cache');

const minimal_args = [
  '--start-maximized',
  `--disk-cache-dir=${cacheDirectory}`,
  '--no-sandbox', // Skip sandboxing for better performance (requires running as root)
  '--disable-setuid-sandbox', // Disable setuid sandbox (Linux)
  '--disable-dev-shm-usage', // Disable /dev/shm usage (Linux)
  '--disable-accelerated-2d-canvas', // Disable GPU-accelerated 2D canvas rendering
  '--disable-gpu', // Disable GPU acceleration
  '--disable-notifications', // Disable browser notifications
  '--disable-crash-reporter', // Disable crash reporter
  '--disable-default-apps', // Disable default apps
  '--disable-background-networking', // Disable background network requests
  '--disable-sync', // Disable syncing browser data
  '--disable-translate', // Disable webpage translation
  '--disable-web-security', // Disable web security
  '--disable-site-isolation-trials', // Disable site isolation
  '--no-zygote', // Skip spawning of the zygote process
  '--autoplay-policy=user-gesture-required',
  '--disable-background-timer-throttling',
  '--disable-backgrounding-occluded-windows',
  '--disable-breakpad',
  '--disable-client-side-phishing-detection',
  '--disable-component-update',
  '--disable-domain-reliability',
  '--disable-features=AudioServiceOutOfProcess',
  '--disable-hang-monitor',
  '--disable-ipc-flooding-protection',
  '--disable-offer-store-unmasked-wallet-cards',
  '--disable-popup-blocking',
  '--disable-print-preview',
  '--disable-prompt-on-repost',
  '--disable-renderer-backgrounding',
  '--disable-speech-api',
  '--deterministic-fetch',
  '--disable-features=IsolateOrigins',
  '--ignore-gpu-blacklist',
  '--mute-audio',
  '--no-default-browser-check',
  '--no-first-run',
  '--no-pings',
  '--password-store=basic',
  '--use-gl=swiftshader',
  '--use-mock-keychain',
  "--disable-infobars",
  '--disable-setuid-sandbox',
  '--ignore-certificate-errors'
];

const config: PuppeteerLaunchOptions = {
  headless: true,
  devtools: false,
  defaultViewport: { width: 1366, height: 768 },
  args: minimal_args,
  ignoreDefaultArgs: [
    '--disable-extensions',
    '--disable-features=site-per-process'    // Disable site isolation for better performance],
  ],
  ignoreHTTPSErrors: true,
  // logLevel: 'error'
}

export default config;