export default async function fetchJson(url: string, options: object = { method: 'GET' }) {
  const r = await fetch(url, options);
  return await r.json()
}