import { Worker } from "worker_threads";

export default function runWorkerPromise<T>(filePath: string, workerData: any): Promise<T> {
  return new Promise((resolve, reject) => {
    const worker = new Worker(filePath, { workerData: workerData });
    worker.on('message', resolve);
    worker.on('error', (err: Error) => {
      console.log('Worker Error ', filePath, err);
      worker.terminate();
    });
    worker.on('exit', reject);
  });
}