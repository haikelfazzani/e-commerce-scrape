import { join } from "path";

export const BASE_IN_CSV_FILES_PATH = join(process.cwd(), '_private', '_in');
export const BASE_OUT_CSV_FILES_PATH = join(process.cwd(), '_private', '_out');
export const BASE_CANCEL_SCRAPE_FILE_PATH = join(process.cwd(), '_private', '_cancel_scrape');
export const BASE_CANCEL_SCRAPE_LOGS_FILE_PATH = join(process.cwd(), '_private', '_scrape_logs.json');
// workers
export const READ_CSV_OR_XLSX_WORKER = join(process.cwd(), 'helpers', 'utils-workers', 'read-csv-or-xlsx.js');
export const UPDATE_PROGRESS_WORKER = join(process.cwd(), 'helpers', 'utils-workers', 'update-progress-file.js');
export const UPDATE_SET_VARIANT_WORKER = join(process.cwd(), 'helpers', 'utils-workers', 'set-variant');
export const READ_DIR_WORKER = join(process.cwd(), 'helpers', 'utils-workers', 'read-dir');
export const SERVICE_BUILDER_WORKER = join(process.cwd(), 'helpers', 'service-builder.js');

export const services = [
  { name: 'Petit Bateau (Tunisie)', value: 'petit-bateau-tunisie' },
  { name: 'Petit Bateau (France)', value: 'petit-bateau-france' },
  { name: 'Sergent Major', value: 'sergent-major' },
  { name: 'Du pareil au même', value: 'dpam' },
  { name: 'Gimel (Tunisie)', value: 'gimel-tunisie' },
  { name: 'Toopty', value: 'toopty' },
  { name: 'Gioseppo (Tunisie)', value: 'gioseppotunisie' },
  { name: 'Kiabi', value: 'kiabi' },
  { name: 'Birkenstock', value: 'birkenstock' },
  { name: 'Mayoral', value: 'mayoral' },
  { name: 'Okaidi', value: 'okaidi' },
  { name: 'Citymode', value: 'citymode' },
];