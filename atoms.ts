import { atom } from "jotai";
import { Product } from "./types";
import { atomWithStorage } from 'jotai/utils'

export const formScrapeAtom = atom({ service: 'petit-bateau-tunisie', inFile: 'petit-bateau_test.xlsx', outFile: '', delay: 10, limitFrom: 0, limitTo: 0, headless: true, cancel: false });

export const asideAtom = atom(false);

export const productsAtom = atom([]);

export const inputFilesAtom = atom([]);

export const inProductsAtom = atom([]);

export const csvFileColumnsAtom = atom([]);

export const isScrapingAtom = atom(false);

export const isScrapingCanceledAtom = atom(false);

export const showTableSelectedItemImageAtom = atom(false);

export const tableSelectedItemAtom = atom<Product>({ nom: '', categorie: '', images: '', taille: '', couleur: '', description: '', id: 0, reference: '', prix: 0 });

export const progressAtom = atom({ columns: [], message: '', products: [], done: false, outFile: '' });

export const scrapeHistoryAtom = atomWithStorage('scrape-history', []);